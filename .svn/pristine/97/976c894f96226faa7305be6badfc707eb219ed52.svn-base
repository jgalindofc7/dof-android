package com.dof.gesformexico.dofsegob.Retrofit;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 */

public class MyGsonBuilder {

    public static GsonBuilder getGsonBuilder() {
        GsonBuilder builder = new GsonBuilder();

        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        JsonSerializer<Date> serializer = new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
                if (src != null) {
                    String fechaStr = dateFormat.format(src);
                    return new JsonPrimitive(fechaStr);
                } else {
                    return null;
                }
            }
        };

        JsonDeserializer<Date> deserializer = new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                if (json != null) {
                    String fechaCad = json.getAsString();
                    try {
                        return dateFormat.parse(fechaCad);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }
        };

        builder.registerTypeAdapter(Date.class, serializer);
        builder.registerTypeAdapter(java.sql.Date.class, serializer);

        builder.registerTypeAdapter(Date.class, deserializer);
        builder.registerTypeAdapter(java.sql.Date.class, deserializer);
        return builder;
    }
}
