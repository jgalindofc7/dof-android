package com.dof.gesformexico.dofsegob.Alertas;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.Modelo.GSAlerta;
import com.dof.gesformexico.dofsegob.Modelo.GSAlertaUsuario;
import com.dof.gesformexico.dofsegob.Modelo.GSCAlertaUsuario;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Jose Cruz Galindo Martinez on 04/04/2017.
 */
class AlertasAdapter extends RecyclerView.Adapter<AlertasAdapter.AlertasViewHolder> {

    private List<GSAlerta> alertas;
    private LayoutInflater mInflater;

    AlertasAdapter(Activity actividad, List<GSAlerta> lAlertas) {
        alertas = lAlertas;
        mInflater = (LayoutInflater) actividad.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public AlertasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = mInflater.inflate(R.layout.item_alerta, parent, false);
        return new AlertasViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(final AlertasViewHolder holder, int position) {
        final GSAlerta selected = alertas.get(position);
        holder.nombreAlerta.setText(selected.textoBusqueda);
        final GSAlertaUsuario alertaUsr = selected.alertaUsuario;
        if (alertaUsr != null && alertaUsr.codEstatus == 1) {
            holder.chkbx.setChecked(true);
        } else {
            holder.chkbx.setChecked(false);
        }

        holder.chkbx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) {
                    Log.d("ITEM_ALERTA", "debo habilitar");
                    if (alertaUsr == null) {
                        inscribirAlerta(selected, holder);
                    } else {
                        actualizarAlerta(alertaUsr, true, holder);
                    }
                } else {
                    Log.d("ITEM_ALERTA", "debo de deshabilitar");
                    actualizarAlerta(alertaUsr, false, holder);
                }
            }
        });
    }

    private void actualizarAlerta(GSAlertaUsuario alerta, final boolean activar, final AlertasViewHolder vista) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);
        GSCAlertaUsuario nAlerta = new GSCAlertaUsuario();
        nAlerta.sid = alerta.sid;
        nAlerta.codEstatus = activar? "1" : "2";
        nAlerta.fechaFin = c.getTime();
        Api.getSharedInstance().actualizarAlerta(nAlerta, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()){
                    Log.d("ITEM_ALERTA", "Actualizar exitoso");
                } else {
                    Log.d("ITEM_ALERTA", "Actualizar no exitoso");
                }
                vista.chkbx.setChecked(activar);
            }
        });
    }

    private void inscribirAlerta(GSAlerta alerta, final AlertasViewHolder vista) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);
        GSCAlertaUsuario nAlerta = new GSCAlertaUsuario();
        nAlerta.sidAlerta = alerta.sid;
        nAlerta.descripcion = alerta.textoBusqueda;
        nAlerta.codEstatus = "1";
        nAlerta.fechaFin = c.getTime();

        Api.getSharedInstance().suscribirAlertaUsuario(nAlerta, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    vista.chkbx.setChecked(true);
                } else {
                    vista.chkbx.setChecked(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return alertas.size();
    }

    class AlertasViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_alerta_texto)
        TextView nombreAlerta;
        @BindView(R.id.item_alerta_checkb)
        CheckBox chkbx;

        AlertasViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
