package com.dof.gesformexico.dofsegob.Cotizador;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dof.gesformexico.dofsegob.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author José Cruz Galindo Martínez on 25/02/17.
 */

public class ConsultorCotizadorFragment extends Fragment {

    @BindView(R.id.fragment_consultor_cotizador_text_in)
    EditText codigoReferencia;

    public ConsultorCotizadorFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_consultor_cotizador, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.fragment_consultor_cotizacion_enviar_btn)
    public void mandarCotizacion() {
        if (datosValidos()) {

        }
    }

    private boolean datosValidos() {
        return !codigoReferencia.getText().toString().equals("");
    }
}
