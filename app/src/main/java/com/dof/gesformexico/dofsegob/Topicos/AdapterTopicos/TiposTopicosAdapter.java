package com.dof.gesformexico.dofsegob.Topicos.AdapterTopicos;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSTiposTopicos;
import com.dof.gesformexico.dofsegob.R;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 07/02/17.
 */
public class TiposTopicosAdapter extends RecyclerView.Adapter<TiposTopicosAdapter.TiposTopicosVH> {

    private LayoutInflater inflater;
    private List<GSTiposTopicos> tiposTopicos;
    private TiposTopicosListener tiposTopicosListener;

    public TiposTopicosAdapter(Activity actividad, List<GSTiposTopicos> lista){
        inflater = (LayoutInflater) actividad.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tiposTopicos = lista;
    }

    @Override
    public TiposTopicosVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.tipos_topicos_item, parent, false);
        return new TiposTopicosVH(v);
    }

    @Override
    public void onBindViewHolder(TiposTopicosVH holder, int position) {
        final GSTiposTopicos selectd = tiposTopicos.get(position);
        holder.tituloTopico.setText(selectd.value);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiposTopicosListener.descargarTopicos(selectd.id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tiposTopicos.size();
    }

    public void setListener(TiposTopicosListener listener) {
        this.tiposTopicosListener = listener;
    }

    public interface TiposTopicosListener {
        void descargarTopicos(String idTT);
    }

    class TiposTopicosVH extends RecyclerView.ViewHolder {

        TextView tituloTopico;

        TiposTopicosVH(View itemView) {
            super(itemView);
            tituloTopico = (TextView) itemView.findViewById(R.id.tipos_topicos_text);
        }
    }

}