package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by José Cruz Galindo Martínez on 31/01/17.
 */

public class GSNotaContainer {

    private int messageCode;
    private String response;
    @SerializedName("Nota")
    private GSNota nota;

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public GSNota getNota() {
        return nota;
    }

    public void setNota(GSNota nota) {
        this.nota = nota;
    }
}
