package com.dof.gesformexico.dofsegob.Utils;

/**
 * @author José Cruz Galindo Martínez on 29/12/16.
 */

public class Constants {

//    public static final String URL_BASE = "http://10.2.175.76:8089"; //Local
//    public static final String URL_BASE = "http://10.2.175.82:8081"; //Hector
    public static final String URL_BASE = "https://sidofqa.segob.gob.mx";
    public static final String SHARE_URL = "https://sidofqa.segob.gob.mx";

    // Parcer de Notas
    public static final String DESERIALIZE_NOTA = "DESERIALIZAR_NOTA";
    public static final String DESERIALIZAR_LISTA_SECCIONES = "DESERIALIZAR_LISTA_SECCIONES";
    public static final String DESERIALIZAR_TITULO = "DESERIALIZAR_TITULO";
    public static final String DESERIALIZAR_NOTAS_TOPICOS = "DESEREALIZAR_NOTAS_TOPICOS";
    public static final String DESERIALIZAR_FECHA = "DESEREALIZAR_FECHA";
    public static final String DESERIALIZAR_INDICADORES = "DESEREALIZAR_INDICADORES";
    public static final String DESERIALIZAR_INDICADOR_BUSQ = "DESEREALIZAR_INDICADOR_BUS";

}
