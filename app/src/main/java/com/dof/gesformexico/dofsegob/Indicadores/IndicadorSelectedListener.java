package com.dof.gesformexico.dofsegob.Indicadores;

import java.util.Date;

/**
 * @author José Cruz Galindo Martínez on 24/04/17.
 */
public interface IndicadorSelectedListener {
    void onFechaPressed(Date fecha);
}
