package com.dof.gesformexico.dofsegob;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dof.gesformexico.dofsegob.Alertas.AlertasFragment;
import com.dof.gesformexico.dofsegob.Contacto.ContactoActivity;
import com.dof.gesformexico.dofsegob.Cotizador.CotizadorActivity;
import com.dof.gesformexico.dofsegob.Favoritos.FavoritosFragment;
import com.dof.gesformexico.dofsegob.Home.HomeFragment;
import com.dof.gesformexico.dofsegob.Indicadores.IndicadoresActivity;
import com.dof.gesformexico.dofsegob.Tramites.TramiteFragment;

/**
 * Created by Gersfor on 18/12/16.
 * @author Francisco Javier  Flores Morales
 * @author Jose Cruz Galindo Martinez
 */
public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.main_content_toolbar);
        setSupportActionBar(myToolbar);

        myToolbar.setVisibility(View.VISIBLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        configurarHeader();
        setFragment();
    }

    private void configurarHeader() {
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment fragment = null;
                        Intent intent = null;
                        switch (item.getItemId()) {
                            case R.id.nav_inicio:
                                fragment = new HomeFragment();
                                break;
                            case R.id.nav_indicadores:
                                intent = new Intent(MainActivity.this, IndicadoresActivity.class);
                                break;
                            case R.id.nav_tramites:
                                fragment = new TramiteFragment();
                                break;
                            case R.id.nav_favoritos:
                                fragment = new FavoritosFragment();
                                break;
                            case R.id.nav_contacto:
                                intent = new Intent(MainActivity.this, ContactoActivity.class);
                                break;
                            case R.id.nav_cotizacion:
                                intent = new Intent(MainActivity.this, CotizadorActivity.class);
                                break;
                            case R.id.nav_alertas:
                                fragment = new AlertasFragment();
                                break;
                        }

                        if (fragment != null) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.contenido_principal_frame, fragment)
                                    .commit();

                            item.setChecked(true);
                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setTitle(item.getTitle());
                            }
                        } else {
                            startActivity(intent);
                        }

                        drawerLayout.closeDrawers();
                        return true;
                    }
                }
        );
    }

    private void setFragment() {
        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new HomeFragment();
        fm.replace(R.id.contenido_principal_frame, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}