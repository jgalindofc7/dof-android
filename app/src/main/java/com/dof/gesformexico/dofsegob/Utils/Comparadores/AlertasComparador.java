package com.dof.gesformexico.dofsegob.Utils.Comparadores;

import com.dof.gesformexico.dofsegob.Modelo.GSAlerta;

import java.util.Comparator;

/**
 * @author Jose Cruz Galindo Martinez on 04/04/2017.
 */

public class AlertasComparador implements Comparator<GSAlerta> {

    @Override
    public int compare(GSAlerta o1, GSAlerta o2) {
        if (o1.sid < o2.sid) {
            return -1;
        }
        if (o1.sid == o2.sid) {
            return 0;
        } else {
            return 1;
        }
    }
}
