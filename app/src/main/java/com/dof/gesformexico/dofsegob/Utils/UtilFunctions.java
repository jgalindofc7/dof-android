package com.dof.gesformexico.dofsegob.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author José Cruz Galindo Martínez on 09/02/17.
 */

public class UtilFunctions {

    public static String stringFromFecha(Date fecha, String estilo) {
        SimpleDateFormat df = new SimpleDateFormat(estilo, Locale.US);
        return df.format(fecha);
    }
}
