package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 17/04/17.
 */
public class GSDiariosContainer {

    private int messageCode;
    private String response;
    @SerializedName("ListaDiarios")
    private List<GSNota> listaDiarios;
    @SerializedName("FechasSinPublicacion")
    private List<Date> fechasSinPublicacion;
}
