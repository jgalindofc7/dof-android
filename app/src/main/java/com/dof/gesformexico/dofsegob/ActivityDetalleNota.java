package com.dof.gesformexico.dofsegob;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * Created by Gersfor on 18/12/16.
 * Francisco Javier Flores Morales
 * @author Jose Cruz Galindo Martinez
 */
public class ActivityDetalleNota extends AppCompatActivity {

    @BindView(R.id.actvty_detalle_nota_dependencia)
    TextView etTitulo;
    @BindView(R.id.actvty_detalle_nota_fecha)
    TextView fechaTxt;
    @BindView(R.id.actvty_detalle_nota_contenido)
    WebView notaContenido;
    @BindView(R.id.detalle_nota_fab_favorito)
    FloatingActionButton favBtn;

    // Logic
    private GSNota notaSelected;
    private DateFormat sdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_notas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_content_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        sdf = new SimpleDateFormat("dd-MMMM-yyyy", new Locale("es", "MX"));
        notaSelected = this.getIntent().getParcelableExtra(Constants.DESERIALIZE_NOTA);
        ButterKnife.bind(this);
        setContenido();
    }

    private void setContenido() {
        if (this.notaSelected != null) {
            String orga = notaSelected.codOrgaDos != null? notaSelected.codOrgaDos :
                    notaSelected.codOrgaUno;
            etTitulo.setText(orga);
            fechaTxt.setText(sdf.format(notaSelected.fecha));
            String cadFormato = notaSelected.cadenaContenido;
            notaContenido.loadData(cadFormato, "text/html; charset=utf-8", "UTF-8");
            notaContenido.getSettings().setDefaultFontSize(25);

            if (notaSelected.favorito) {
                favBtn.setIcon(R.drawable.ic_star);
            } else {
                favBtn.setIcon(R.drawable.ic_star_outline);
            }
        }
    }

    @OnClick(R.id.detalle_nota_fab_favorito)
    void administrarFavoritos() {
        Log.d("DetalleNota", "Voy a guardar la nota");
        if (notaSelected.favorito) {
            notaSelected.delete();
            notaSelected.favorito = false;
            favBtn.setIcon(R.drawable.ic_star_outline);
        } else {
            notaSelected.favorito = true;
            notaSelected.save();
            favBtn.setIcon(R.drawable.ic_star);
        }
    }

    @OnClick(R.id.detalle_nota_fab_compartir)
    void compartirNota() {
        Log.d("DetalleNota","compartir");
        Intent sIntent = new Intent();
        sIntent.setAction(Intent.ACTION_SEND);
        String shareStr = String.format(new Locale("es", "MX"),
                "%s/notas/%d", Constants.SHARE_URL, notaSelected.codNota);
        sIntent.putExtra(Intent.EXTRA_SUBJECT, "Nota");
        sIntent.putExtra(Intent.EXTRA_TEXT, shareStr);
        sIntent.setType("text/plain");
        startActivity(Intent.createChooser(sIntent, "Compartir"));
    }

    @OnClick(R.id.detalle_nota_fab_descargar)
    void guardarDoc() {
        final String codNota = String.valueOf(notaSelected.codNota);
        Api.getSharedInstance().descargarDocumentoDoc(codNota, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    File path = Environment.getExternalStorageDirectory();
                    String fechaStr = "Nota-" + codNota + ".doc";
                    File file = new File(path, fechaStr);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        ResponseBody resBody = (ResponseBody)resultado.getObjeto();
                        fileOutputStream.write(resBody.bytes());
                        fileOutputStream.close();
                        Toast.makeText(ActivityDetalleNota.this, "Descarga Terminada", Toast.LENGTH_SHORT).show();
                    } catch (IOException ioe) {
                        Log.e("ActivityDetalleNota", ioe.getLocalizedMessage());
                    }
                } else {
                    Log.e("ActivityDetalleNota", "Error al descargar DOC");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}