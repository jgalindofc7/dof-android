package com.dof.gesformexico.dofsegob.Api;

import com.dof.gesformexico.dofsegob.Modelo.GSAlertaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSBusquedaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCAlertaUsuario;
import com.dof.gesformexico.dofsegob.Modelo.GSCatalogoContactoContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCotizacionContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCotizador;
import com.dof.gesformexico.dofsegob.Modelo.GSDiariosContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSEnvioContacto;
import com.dof.gesformexico.dofsegob.Modelo.GSEnvioQuejas;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicadorContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasTopicosContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicadorContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTopicosContainer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * @author José Cruz Galindo Martínez on 29/12/16.
 */
interface DofService {

    @GET("/dof/sidof/indicadores/{fecha}")
    Call<GSIndicadorContainer> getIndicadores(@Path("fecha") String fecha);

    @GET("/dof/sidof/catalogos/cat_tipos_indicadores")
    Call<GSTipoIndicadorContainer> getTipoIndicadores();

    //
    @GET("/dof/sidof/indicadores/{tipoIndicador}/{fechaInicio}/{fechaFin}")
    Call<GSIndicadorContainer> getIndicadoresRango(@Path("tipoIndicador") String tipoIndicador, @Path("fechaInicio") String fechaInicio,@Path("fechaFin") String fechaFin);

    //{ruta}/dof/sidof/notas/{fecha} -> fecha = "11-10-2016"
    @GET("/dof/sidof/notas/{fecha}")
    Call<GSNotasContainer> getNotaContainer(@Path("fecha") String fecha);

    //@"/dof/sidof/catalogos/cat_topicos";
    @GET("/dof/sidof/catalogos/cat_topicos")
    Call<GSTopicosContainer> getCatTopicos();

    ///dof/sidof/notasTopicos/movil/{idTopico}/{pagina}/{orden}
    @GET("/dof/sidof/notasTopicos/movil/{idTopico}/{pagina}/{sort}/{orden}")
    Call<GSNotasTopicosContainer> getNotasTopicos(@Path("idTopico") String idTopico,
                                                  @Path("pagina") String pagina,
                                                  @Path("sort") String sorting,
                                                  @Path("orden") String orden);

    // dof/sidof/notas/nota/id
    @GET("/dof/sidof/notas/nota/{idNota}")
    Call<GSNotaContainer> getDetalleNota(@Path("idNota") long idNota);


    @POST("/dof/sidof/cotizador")
    Call<GSCotizacionContainer> mandarCotizacion(@Body GSCotizador cotizador);

    @GET("/dof/sidof/buscarNotas/avanzada/T/{titulo}/{fechaI}/{fechaF}/todos")
    Call<GSBusquedaContainer> realizarBusquedaAvanzada(@Path("titulo") String titulo,
                                                       @Path("fechaI") String fechaInicio,
                                                       @Path("fechaF") String fechaFin);

    // Alertas
    @GET("/dof/sidof/alertas/obtieneAlertasPublicasPorUsuario/{usuario}")
    Call<GSAlertaContainer> obtenerAlertasUsuario(@Path("usuario") String usuario);

    @POST("dof/sidof/usuarioAlertas/usuarioAlertas")
    Call<GSAlertaContainer> suscribirAlerta(@Body GSCAlertaUsuario alerta);

    @PUT("/dof/sidof/usuarioAlertas/usuarioAlertas")
    Call<GSAlertaContainer> renovarAlerta(@Body GSCAlertaUsuario alerta);

    @GET("/dof/sidof/documentos/pdf/{codDiario}")
    Call<ResponseBody> descargarPDF(@Path("codDiario") String codigoDiario);

    @GET("/dof/sidof/documentos/doc/{codNota}")
    Call<ResponseBody> descargarDoc(@Path("codNota") String codigoNota);

    @GET("dof/sidof/diarios/{anio}")
    Call<GSDiariosContainer> obtenerDiarios(@Path("anio") String anio);

    @GET("dof/sidof/catalogos/cat_quejas_contacto")
    Call<GSCatalogoContactoContainer> obtenerCatalogoContacto();

    @GET("dof/sidof/catalogos/cat_areas")
    Call<GSCatalogoContactoContainer> obtenerCatalogoAreas();

    @POST("dof/sidof/quejas/envioQuejas")
    Call<ResponseBody> mandarSugerencia(@Body GSEnvioQuejas quejas);

    @POST("dof/sidof/consultas/envioConsultas")
    Call<ResponseBody> mandarContacto(@Body GSEnvioContacto contacto);

}
