package com.dof.gesformexico.dofsegob.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * @author José Cruz Galindo Martínez on 16/03/17.
 */
public class TituloTextView extends android.support.v7.widget.AppCompatTextView {


    public TituloTextView(Context context) {
        super(context);
        init();
    }

    public TituloTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TituloTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/TrajanProBold.ttf");
        setTypeface(tf);
    }
}
