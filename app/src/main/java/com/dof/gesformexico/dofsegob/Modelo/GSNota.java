package com.dof.gesformexico.dofsegob.Modelo;

import android.os.Parcel;
import android.os.Parcelable;

import com.dof.gesformexico.dofsegob.DB.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

/**
 * Created by Gersfor on 16/01/17.
 * GSNota es la representacion de una nota del
 * diario oficial.
 */
@Table(database = AppDatabase.class)
public class GSNota extends BaseModel implements Parcelable {

    @PrimaryKey
    @Column
    public long codNota;

    @Column
    public String titulo;

    @Column
    public String codSeccion;

    @Column
    public Date fecha;

    @Column
    public long codDiario;

    @Column
    public String cadenaContenido;

    @Column
    public boolean favorito;

    public String existeDoc;
    public String existeImagen;
    public String codEdicion;
    public String tipoNota;
    public int  estado;
    public int paginaHasta;
    public int tiempoLectura;

    @Column
    public String codOrgaUno;

    @Column
    public String codOrgaDos;

    @Column
    public String codOrgaTres;
    public String codOrgaCuatro;

    @Column
    public String nomOrganismo;
    public int pagina;
    public int orden;


    @Override
    public String toString() {
        return "GSNota{" +
                "codNota=" + codNota +
                ", titulo='" + titulo + '\'' +
                ", codSeccion='" + codSeccion + '\'' +
                ", fecha=" + fecha +
                ", codDiario=" + codDiario +
                ", cadenaContenido='" + cadenaContenido + '\'' +
                ", favorito=" + favorito +
                ", existeDoc='" + existeDoc + '\'' +
                ", existeImagen='" + existeImagen + '\'' +
                ", codEdicion='" + codEdicion + '\'' +
                ", tipoNota='" + tipoNota + '\'' +
                ", estado=" + estado +
                ", paginaHasta=" + paginaHasta +
                ", tiempoLectura=" + tiempoLectura +
                ", codOrgaUno='" + codOrgaUno + '\'' +
                ", codOrgaDos='" + codOrgaDos + '\'' +
                ", codOrgaTres='" + codOrgaTres + '\'' +
                ", codOrgaCuatro='" + codOrgaCuatro + '\'' +
                ", nomOrganismo='" + nomOrganismo + '\'' +
                ", pagina=" + pagina +
                ", orden=" + orden +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.codNota);
        dest.writeString(this.titulo);
        dest.writeString(this.codSeccion);
        dest.writeLong(this.fecha != null ? this.fecha.getTime() : -1);
        dest.writeLong(this.codDiario);
        dest.writeString(this.cadenaContenido);
        dest.writeByte(this.favorito ? (byte) 1 : (byte) 0);
        dest.writeString(this.existeDoc);
        dest.writeString(this.existeImagen);
        dest.writeString(this.codEdicion);
        dest.writeString(this.tipoNota);
        dest.writeInt(this.estado);
        dest.writeInt(this.paginaHasta);
        dest.writeInt(this.tiempoLectura);
        dest.writeString(this.codOrgaUno);
        dest.writeString(this.codOrgaDos);
        dest.writeString(this.codOrgaTres);
        dest.writeString(this.codOrgaCuatro);
        dest.writeString(this.nomOrganismo);
        dest.writeInt(this.pagina);
        dest.writeInt(this.orden);
    }

    public GSNota() {
    }

    protected GSNota(Parcel in) {
        this.codNota = in.readLong();
        this.titulo = in.readString();
        this.codSeccion = in.readString();
        long tmpFecha = in.readLong();
        this.fecha = tmpFecha == -1 ? null : new Date(tmpFecha);
        this.codDiario = in.readLong();
        this.cadenaContenido = in.readString();
        this.favorito = in.readByte() != 0;
        this.existeDoc = in.readString();
        this.existeImagen = in.readString();
        this.codEdicion = in.readString();
        this.tipoNota = in.readString();
        this.estado = in.readInt();
        this.paginaHasta = in.readInt();
        this.tiempoLectura = in.readInt();
        this.codOrgaUno = in.readString();
        this.codOrgaDos = in.readString();
        this.codOrgaTres = in.readString();
        this.codOrgaCuatro = in.readString();
        this.nomOrganismo = in.readString();
        this.pagina = in.readInt();
        this.orden = in.readInt();
    }

    public static final Parcelable.Creator<GSNota> CREATOR = new Parcelable.Creator<GSNota>() {
        @Override
        public GSNota createFromParcel(Parcel source) {
            return new GSNota(source);
        }

        @Override
        public GSNota[] newArray(int size) {
            return new GSNota[size];
        }
    };
}
