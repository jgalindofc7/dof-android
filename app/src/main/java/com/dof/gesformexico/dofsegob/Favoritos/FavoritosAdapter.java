package com.dof.gesformexico.dofsegob.Favoritos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.ActivityDetalleNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author José Cruz Galindo Martínez on 21/04/17.
 */
class FavoritosAdapter extends RecyclerView.Adapter<FavoritosAdapter.FavoritoVH> {

    private List<GSNota> notasVisibles;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es", "MX"));

    FavoritosAdapter(Activity activity, List<GSNota> notas){
        mActivity = activity;
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        notasVisibles = notas;
    }

    @Override
    public FavoritoVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = mInflater.inflate(R.layout.notas_item, parent, false);
        return new FavoritoVH(vista);
    }

    @Override
    public void onBindViewHolder(FavoritoVH holder, int position) {
        final GSNota slctd = notasVisibles.get(position);
        holder.titulo.setText(slctd.titulo);
        String orga = slctd.codOrgaDos != null? slctd.codOrgaDos : slctd.codOrgaUno;
        holder.dependencia.setText(orga);
        holder.fecha.setText(sdf.format(slctd.fecha));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ActivityDetalleNota.class);
                intent.putExtra(Constants.DESERIALIZE_NOTA, slctd);
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notasVisibles.size();
    }

    class FavoritoVH extends RecyclerView.ViewHolder {

        @BindView(R.id.notas_item_titulo)
        TextView titulo;
        @BindView(R.id.notas_item_dependencia)
        TextView dependencia;
        @BindView(R.id.notas_item_fecha)
        TextView fecha;

        FavoritoVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
