package com.dof.gesformexico.dofsegob.Sumario;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.dof.gesformexico.dofsegob.Utils.UtilFunctions;
import com.dof.gesformexico.dofsegob.fecha.CustomDatePicker;
import com.dof.gesformexico.dofsegob.Sumario.Adapters.*;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author José Cruz Galindo Martínez on 09/02/17.
 */
public class SumarioFragment extends FragmentoBase
        implements DatePickerDialog.OnDateSetListener {

    // UI
//    private SumarioAdapter mAdapter;

    // Logic
    private GSNotasContainer sumario;
    private List<String> seccionesMatutinas;
    private List<String> seccionesVespertinas;
    private List<String> seccionesExtraordinarias;
    // Notas por seccion
    private HashMap<String, List<GSNota>> notasMatutinas;
    private HashMap<String, List<GSNota>> notasVespertinas;
    private HashMap<String, List<GSNota>> notasExtraordinarias;

    public static final int EDICION_MATUTINA = 0;
    public static final int EDICION_VESPERTINA = 1;
    public static final int EDICION_EXTRAORDINARIA = 2;
    private final String TAG = SumarioFragment.class.getName();

    public SumarioFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_simple_recycler_view, container, false);
        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_simple_recyclerview);

        seccionesMatutinas = new ArrayList<>();
        seccionesVespertinas = new ArrayList<>();
        seccionesExtraordinarias = new ArrayList<>();

        notasMatutinas = new HashMap<>();
        notasVespertinas = new HashMap<>();
        notasExtraordinarias = new HashMap<>();

//        mAdapter = new SumarioAdapter(getActivity(), seccionesMatutinas,
//                seccionesVespertinas, seccionesExtraordinarias);
//        mAdapter.setContainer(this);
//        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Date hoy = new Date();
        actualizarNotas(hoy);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sumario, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sumario_download:
                return true;
            case R.id.menu_sumario_calendario:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mostrarCalendarioForN();
                } else {
                    mostrarCalendarioForL();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void actualizarNotas(Date fecha) {
        mostrarDialogo("Cargando Datos");
        String fechaSlctd = UtilFunctions.stringFromFecha(fecha, "dd-MM-yyyy");
        Api.getSharedInstance().obtenerNotas(fechaSlctd, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    sumario = (GSNotasContainer)resultado.getObjeto();
                    crearSeccionesV2();
                }
                ocultarDialogo();
            }
        });
    }

    private void crearSeccionesV2() {
        if (sumario != null) {
            notasMatutinas.clear();
            for (GSNota nota : sumario.getNotasMatutinas()) {
                String llave = nota.codSeccion;
                if (nota.titulo != null) {
                    if (notasMatutinas.containsKey(llave)) {
                        notasMatutinas.get(llave).add(nota);
                    } else {
                        List<GSNota> notasBySection = new LinkedList<>();
                        notasBySection.add(nota);
                        notasMatutinas.put(llave, notasBySection);
                    }
                }
            }
            
            seccionesMatutinas.clear();
            seccionesMatutinas.addAll(notasMatutinas.keySet());

            notasVespertinas.clear();
            for (GSNota nota : sumario.getNotasVespertinas()) {
                String llave = nota.codSeccion;
                if (nota.titulo != null) {
                    if (notasVespertinas.containsKey(llave)) {
                        notasVespertinas.get(llave).add(nota);
                    } else {
                        List<GSNota> notasBySection = new LinkedList<>();
                        notasBySection.add(nota);
                        notasVespertinas.put(llave, notasBySection);
                    }
                }
            }

            seccionesVespertinas.clear();
            seccionesVespertinas.addAll(notasVespertinas.keySet());

            notasExtraordinarias.clear();
            for (GSNota notaE : sumario.getNotasExtraodinarias()) {
                String llave = notaE.codSeccion;
                if (notaE.titulo != null) {
                    if (notasExtraordinarias.containsKey(llave)) {
                        notasExtraordinarias.get(llave).add(notaE);
                    } else {
                        List<GSNota> notasBySection = new LinkedList<>();
                        notasBySection.add(notaE);
                        notasExtraordinarias.put(llave, notasBySection);
                    }
                }
            }

            seccionesExtraordinarias.clear();
            seccionesExtraordinarias.addAll(notasExtraordinarias.keySet());

//            mAdapter.notifyDataSetChanged();
        }
    }

    protected void crearSecciones() {
        if (sumario != null) {
            Set<String> conjunto = new TreeSet<>();
            // Creamos las secciones Matutinas
            for (GSNota nota: sumario.getNotasMatutinas()) {
                conjunto.add(nota.codSeccion);
            }
            seccionesMatutinas.clear();
            for (String str: conjunto) {
                seccionesMatutinas.add(str);
            }

            // Creamos las secciones Vespertinas
            conjunto.clear();
            for (GSNota nota: sumario.getNotasVespertinas()) {
                conjunto.add(nota.codSeccion);
            }
            seccionesVespertinas.clear();
            for (String str: conjunto) {
                seccionesVespertinas.add(str);
            }

            // Creamos las secciones Extraordinarias
            conjunto.clear();
            for (GSNota nota: sumario.getNotasExtraodinarias()) {
                conjunto.add(nota.codSeccion);
            }
            seccionesExtraordinarias.clear();
            for (String str: conjunto) {
                seccionesExtraordinarias.add(str);
            }
//            mAdapter.notifyDataSetChanged();
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void mostrarCalendarioForN() {
        DatePickerDialog datePicker = new DatePickerDialog(getContext());
        datePicker.setOnDateSetListener(this);
        datePicker.show();
    }

    private void mostrarCalendarioForL() {
        CustomDatePicker datePickerF = new CustomDatePicker();
        datePickerF.setListener(this);
        datePickerF.show(getActivity().getFragmentManager(), "Seleccionar Fecha");
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        actualizarNotas(c.getTime());
    }

    public void mostrarNotasPorSeccion(int edicion, String seccion) {
        Log.d(TAG, seccion);
        String contenido = "";
        List<GSNota> tmp;
        switch (edicion) {
            case EDICION_MATUTINA:
                tmp = notasMatutinas.get(seccion);
                contenido = new Gson().toJson(tmp);
                break;
            case EDICION_VESPERTINA:
                tmp = notasVespertinas.get(seccion);
                contenido = new Gson().toJson(tmp);
                break;
            case EDICION_EXTRAORDINARIA:
                tmp = notasExtraordinarias.get(seccion);
                contenido = new Gson().toJson(tmp);
                break;
            default:
                break;
        }

        Intent seccionesIntent = new Intent(getActivity(), SumarioListActivity.class);
        seccionesIntent.putExtra(Constants.DESERIALIZAR_LISTA_SECCIONES, contenido);
        seccionesIntent.putExtra(Constants.DESERIALIZAR_TITULO, seccion);

        getActivity().startActivity(seccionesIntent);

//        getActivity().getSupportFragmentManager()
//                .beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .addToBackStack(TAG)
//                .add(R.id.contenido_principal_frame, fragment)
//                //.beginTransaction().add(fragment, "Lista")
//                .commit();
    }
}
