package com.dof.gesformexico.dofsegob.Sumario;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.ActivityDetalleNota;
import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNotaContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Sumario.Adapters.SeccionAdapter;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 10/02/17.
 */
public class NotasFragment extends FragmentoBase {

    private List<GSNota> notas;
    //private final String TAG = NotasFragment.class.getName();
    private SeccionAdapter mAdapter;

    public NotasFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        notas = new LinkedList<>();
        Bundle mBundle = this.getArguments();
        if (mBundle != null) {
            String listStr = mBundle.getString(Constants.DESERIALIZAR_LISTA_SECCIONES);
            Gson gson = new Gson();
            GSNota[] tmp = gson.fromJson(listStr, GSNota[].class);
            notas.addAll(Arrays.asList(tmp));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_simple_recycler_view, container, false);
        RecyclerView mRecycler = (RecyclerView) root.findViewById(R.id.fragment_simple_recyclerview);

//        mAdapter =  new SeccionAdapter(getActivity(), notas);
//        mAdapter.setListener(this);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        setHasOptionsMenu(false);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter.notifyDataSetChanged();
    }


//    @Override
//    public void mandarNotaPorSeccion(GSNota notaSelected) {
//        final NotasFragment weakMe = this;
//        mostrarDialogo("Cargando Datos");
//        Api.getSharedInstance().obtenerDetalleNota(notaSelected.codNota, new TaskCallback() {
//            @Override
//            public void processResult(ResultadoApi resultado) {
//                if (resultado.isExito()) {
//                    GSNotaContainer wrapper = (GSNotaContainer)resultado.getObjeto();
//                    String notaStr = new Gson().toJson(wrapper.getNota());
//                    Intent intent = new Intent(getContext(), ActivityDetalleNota.class);
//                    intent.putExtra(Constants.DESERIALIZE_NOTA, notaStr);
//                    getActivity().startActivity(intent);
//                    weakMe.ocultarDialogo();
//                } else {
//                    Toast.makeText(weakMe.getContext(), "No se ha podido recuperar la nota",
//                            Toast.LENGTH_SHORT).show();
//                }
//                weakMe.ocultarDialogo();
//            }
//        });
//    }
}
