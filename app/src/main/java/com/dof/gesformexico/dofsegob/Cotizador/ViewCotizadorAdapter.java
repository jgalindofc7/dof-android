package com.dof.gesformexico.dofsegob.Cotizador;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 25/02/17.
 */
public class ViewCotizadorAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentos = new ArrayList<>(2);
    private String[] titulos = {"COTIZAR", "CONSULTAR"};

    public ViewCotizadorAdapter(FragmentManager fm) {
        super(fm);
        CotizadorFragment fragmentOne = new CotizadorFragment();
        ConsultorCotizadorFragment fragmentTwo = new ConsultorCotizadorFragment();
        fragmentos.add(fragmentOne);
        fragmentos.add(fragmentTwo);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentos.get(position);
    }

    @Override
    public int getCount() {
        return fragmentos.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titulos[position];
    }
}
