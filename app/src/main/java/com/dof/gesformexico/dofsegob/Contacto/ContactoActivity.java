package com.dof.gesformexico.dofsegob.Contacto;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.Modelo.GSCatalogoContacto;
import com.dof.gesformexico.dofsegob.Modelo.GSCatalogoContactoContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSEnvioContacto;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author José Cruz Galindo Martínez on 02/05/17.
 */
public class ContactoActivity extends AppCompatActivity implements OnMapReadyCallback {

    // UI
    @BindView(R.id.contacto_toolbar)
    Toolbar toolbar;
    @BindView(R.id.actvty_contacto_correo)
    EditText correo;
    @BindView(R.id.actvty_contacto_comentarios)
    EditText coments;
    @BindView(R.id.actvty_contacto_mapView)
    MapView mapView;
    @BindView(R.id.actvty_contacto_llegar)
    Button llegarBtn;
    @BindView(R.id.actvty_contacto_spinner)
    Spinner spinnerQuejas;
    @BindView(R.id.actvty_contacto_spinner_cont)
    Spinner spinnerContacto;
    @BindView(R.id.actvt_contacto_quejas)
    Button quejasBtn;
    @BindView(R.id.actvt_contacto_contacto)
    Button contactoBtn;

    // Logic
    private List<GSCatalogoContacto> catContacto;
    private List<GSCatalogoContacto> catAreas;
    private ContactoSpinnerCAdapter mAdapter;
    private ContactoSpinnerCAdapter mAdapter2;
    private GSCatalogoContacto catalogoSelected;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contacto);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        quejasBtn.setSelected(true);
        contactoBtn.setSelected(false);

        mapView.onCreate(savedInstanceState);
        mapView.setVisibility(View.GONE);
        llegarBtn.setVisibility(View.GONE);
        mapView.onResume();
        mapView.getMapAsync(this);

        catContacto = new LinkedList<>();
        catAreas = new LinkedList<>();
        mAdapter = new ContactoSpinnerCAdapter(this, android.R.layout.simple_dropdown_item_1line, catAreas);
        mAdapter2 = new ContactoSpinnerCAdapter(this, android.R.layout.simple_dropdown_item_1line, catContacto);
        spinnerQuejas.setAdapter(mAdapter);
        spinnerContacto.setAdapter(mAdapter2);

        Api.getSharedInstance().descargarCatalogoAreas(new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    catContacto.addAll(((GSCatalogoContactoContainer)resultado.getObjeto()).lista);
                    mAdapter2.notifyDataSetChanged();
                }
            }
        });

        Api.getSharedInstance().descargarCatalogoContacto(new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    catAreas.addAll(((GSCatalogoContactoContainer)resultado.getObjeto()).lista);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private boolean datosValidos() {
        boolean valido = true;
        if (catalogoSelected == null) {
            valido = false;
            Toast.makeText(this, "Debes seleccionar una opción", Toast.LENGTH_SHORT).show();
        }
        if (correo.getText().toString().equals("")) {
            correo.setError("El correo esta vacío.");
        }

        return valido;
    }

    @OnClick(R.id.actvt_contacto_quejas)
    void configurarQuejas() {
        catalogoSelected = null;
        quejasBtn.setSelected(true);
        contactoBtn.setSelected(false);
        llegarBtn.setVisibility(View.GONE);
        mapView.setVisibility(View.GONE);
        spinnerQuejas.setVisibility(View.VISIBLE);
        spinnerContacto.setVisibility(View.GONE);
    }

    @OnClick(R.id.actvt_contacto_contacto)
    void configurarContacto() {
        catalogoSelected = null;
        quejasBtn.setSelected(false);
        contactoBtn.setSelected(true);
        llegarBtn.setVisibility(View.VISIBLE);
        mapView.setVisibility(View.VISIBLE);
        spinnerContacto.setVisibility(View.VISIBLE);
        spinnerQuejas.setVisibility(View.GONE);
    }

    @OnClick(R.id.actvty_contacto_aceptar)
    void mandarComentarios() {
        if (datosValidos()) {
            String objeto2Send = "";
            if (contactoBtn.isSelected()) {
                GSEnvioContacto envio;
                envio = new GSEnvioContacto(correo.getText().toString(), coments.getText().toString());
                envio.setCodConsultaTema(catalogoSelected.id);
                objeto2Send = new Gson().toJson(envio);

                // Lo mando
            } else {
                // Lo mando
            }
        }
    }

    @OnClick(R.id.actvty_contacto_llegar)
    void mostrarComoLlegar() {
        Uri gmmIntentUri = Uri.parse("geo:19.430982,-99.163392?q=19.430982,-99.163392(Diario Oficial)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        double latitu = 19.430982;
        double longit = -99.163392;
        LatLng estacion = new LatLng(latitu, longit);
        googleMap.addMarker(new MarkerOptions().position(estacion).title("DOF"));

        CameraPosition cPosition = new CameraPosition.Builder().target(estacion).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cPosition));
    }
}
