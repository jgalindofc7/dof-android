package com.dof.gesformexico.dofsegob.Busqueda;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Modelo.GSBusquedaContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Sumario.SumarioListActivity;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.dof.gesformexico.dofsegob.fecha.CustomDatePicker;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author José Cruz Galindo Martínez on 22/03/17.
 */
public class BusquedaAvanzadaFragment extends FragmentoBase
        implements DatePickerDialog.OnDateSetListener{

    // UI
    @BindView(R.id.frgmnt_ba_text_in)
    EditText busquedaIn;
    @BindView(R.id.frgmnt_ba_desde_btn)
    Button desdeBtn;
    @BindView(R.id.frgmnt_ba_hasta_btn)
    Button hastaBtn;
    @BindView(R.id.frgmnt_ba_buscar_btn)
    Button busquedaBtn;

    // Logic
    private int dialogSelected = -1;
    private long fechaDesdeSelected = 0;
    private long fechaHastaSelected = 0;
    private DateFormat formatter;
    private final int DIALOG_DESDE = 7;
    private final int DIALOG_HASTA = 17;



    public BusquedaAvanzadaFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formatter = new SimpleDateFormat("dd-MM-yyyy", new Locale("es", "MX"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_busqueda_avanzada, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.frgmnt_ba_desde_btn)
    void mostrarCalendarioDesde() {
        dialogSelected = DIALOG_DESDE;
        mostrarCalendario();
    }

    @OnClick(R.id.frgmnt_ba_hasta_btn)
    void mostrarCalendarioHasta() {
        dialogSelected = DIALOG_HASTA;
        mostrarCalendario();
    }


    private void mostrarCalendario() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            DatePickerDialog datePicker = new DatePickerDialog(getContext());
            datePicker.setOnDateSetListener(this);
            datePicker.show();
        } else {
            CustomDatePicker datePickerF = new CustomDatePicker();
            datePickerF.setListener(this);
            datePickerF.show(getActivity().getFragmentManager(), "Seleccionar Fecha");
        }
    }



    @OnClick(R.id.frgmnt_ba_buscar_btn)
    void busquedaNotas() {
        boolean valido = true;
        if (busquedaIn.getText().toString().equals("")) {
            busquedaIn.setError("Busqueda vacia.");
            valido = false;
        }
        if (fechaDesdeSelected == 0) {
            valido = false;
        }
        if (fechaHastaSelected == 0) {
            valido = false;
        }

        if (valido) {
            // Aqui hacer algo
            mostrarDialogo("Realizando Busqueda");
            Api.getSharedInstance().mandarBusquedaAvanzada(busquedaIn.getText().toString(),
                    fechaDesdeSelected,
                    fechaHastaSelected, new TaskCallback() {
                @Override
                public void processResult(ResultadoApi resultado) {
                    if (resultado.isExito()) {
                        GSBusquedaContainer wrapper = (GSBusquedaContainer) resultado.getObjeto();
                        Intent listaIntent = new Intent(getContext(), SumarioListActivity.class);
                        listaIntent.putExtra(Constants.DESERIALIZAR_LISTA_SECCIONES,
                                new Gson().toJson(wrapper.getNotas()));
                        getActivity().startActivity(listaIntent);
                    } else {
                        Toast.makeText(getContext(), "No se han encontrado resultados.",
                                Toast.LENGTH_SHORT).show();
                    }
                    ocultarDialogo();
                }
            });
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);
        Date hoy = new Date();
        if (c.getTimeInMillis() > hoy.getTime()) {
            c.setTimeInMillis(hoy.getTime());
        }
        if (dialogSelected == DIALOG_DESDE) {
            fechaDesdeSelected = c.getTimeInMillis();
            desdeBtn.setText(formatter.format(c.getTime()));
        } else {
            fechaHastaSelected = c.getTimeInMillis();
            hastaBtn.setText(formatter.format(c.getTime()));
        }
        dialogSelected = -1;
    }

}
