package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

/**
 * @author José Cruz Galindo Martínez on 10/02/17.
 */

public class GSCotizador {

    private String nombre;
    private String apellido;
    private String email;
    private String telefono;
    @SerializedName("nombreArchivo")
    private String fileName;
    @SerializedName("archivo")
    private String archivoB64;
    private String tipoCotizacion;

    public GSCotizador() { }

    public String getNombres() {
        return nombre;
    }

    public void setNombres(String nombres) {
        this.nombre = nombres;
    }

    public String getApellidos() {
        return apellido;
    }

    public void setApellidos(String apellidos) {
        this.apellido = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getArchivoB64() {
        return archivoB64;
    }

    public void setArchivoB64(String archivoB64) {
        this.archivoB64 = archivoB64;
    }

    public String getTipoCotizacion() {
        return tipoCotizacion;
    }

    public void setTipoCotizacion(String tipoCotizacion) {
        this.tipoCotizacion = tipoCotizacion;
    }

    @Override
    public String toString() {
        return "GSCotizador{" +
                "nombres='" + nombre + '\'' +
                ", apellidos='" + apellido + '\'' +
                ", email='" + email + '\'' +
                ", telefono='" + telefono + '\'' +
                ", fileName='" + fileName + '\'' +
                ", archivoB64='" + archivoB64.substring(0,20) + '\'' +
                ", tipoCotizacion='" + tipoCotizacion + '\'' +
                '}';
    }
}
