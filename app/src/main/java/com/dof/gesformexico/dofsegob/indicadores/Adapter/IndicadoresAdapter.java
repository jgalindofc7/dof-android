package com.dof.gesformexico.dofsegob.Indicadores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSIndicador;
import com.dof.gesformexico.dofsegob.R;

import java.util.List;

/**
 * Francisco Javier  Flores Morales
 * fflores.mo@gesfor.com.mx
 * @author José Cruz Galindo Martínez
 */
public class IndicadoresAdapter extends BaseAdapter {

    private List<GSIndicador> listaIndicadores;
    private LayoutInflater mInflater;
    private SparseArray<String> map;

    public IndicadoresAdapter(Activity activity, List<GSIndicador> indicadores,
                              SparseArray<String> diccionario) {
        this.listaIndicadores = indicadores;
        this.mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.map = diccionario;
    }


    @Override
    public int getCount() {
        return listaIndicadores.size();
    }

    @Override
    public Object getItem(int i) {
        return listaIndicadores.get(i);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(R.layout.indicador_item, viewGroup, false);
        }

        IndicadorViewHolder holder = new IndicadorViewHolder();
        holder.nombreIndicador = (TextView)view.findViewById(R.id.indicador_item_nombre);
        holder.valorIndicador = (TextView)view.findViewById(R.id.indicador_item_valor);

        GSIndicador indicador = listaIndicadores.get(i);
        String nombre = map.get((int)indicador.getCodTipoIndicador());
        holder.nombreIndicador.setText(nombre);
        holder.valorIndicador.setText(indicador.getValor());

        view.setTag(holder);

        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class IndicadorViewHolder {
        TextView nombreIndicador;
        TextView valorIndicador;
    }
}
