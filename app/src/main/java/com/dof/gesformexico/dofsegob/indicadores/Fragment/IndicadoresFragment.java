package com.dof.gesformexico.dofsegob.Indicadores.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Indicadores.Adapter.IndicadorDiaAdapter;
import com.dof.gesformexico.dofsegob.Indicadores.IndicadorSelectedListener;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicador;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicadorContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicador;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicadorContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Indicadores.Adapter.IndicadoresAdapter;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Gersfor on 18/12/16.
 * @author Francisco Javier  Flores Morales
 * @email fflores.mo@gesfor.com.mx
 * @author José Cruz Galindo Martínez
 */
public class IndicadoresFragment extends FragmentoBase implements IndicadorSelectedListener {

    // UI
    @BindView(R.id.fragment_indicadores_dia_number)
    TextView diaNumero;
    @BindView(R.id.fragment_indicadores_list)
    ListView listaIndicadores;
    @BindView(R.id.fragment_indicadores_recycler)
    RecyclerView mRecycler;

    // Logic
    private List<GSIndicador> indicadoresContainer;
    private SparseArray<String> hashNombres;
    private IndicadoresAdapter mAdapter;
    private DateFormat formatter;
    private DateFormat tituloFormat;

    public IndicadoresFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        indicadoresContainer = new LinkedList<>();
        hashNombres = new SparseArray<>();
        obtenerCatalogoIndicadores();
        mAdapter = new IndicadoresAdapter(getActivity(), indicadoresContainer, hashNombres);
        formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        tituloFormat = new SimpleDateFormat("EEEE dd 'de' MMMM", new Locale("es", "MX"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_indicadores, container, false);
        ButterKnife.bind(this, vista);
        return  vista;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listaIndicadores.setAdapter(mAdapter);
        Date hoy = Calendar.getInstance().getTime();
        obtenerIndicadores(hoy);
        IndicadorDiaAdapter dAdpter = new IndicadorDiaAdapter(getActivity());
        dAdpter.mListener = this;
        mRecycler.setAdapter(dAdpter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
    }

    private void obtenerIndicadores(Date fecha) {
        String fechaStr = formatter.format(fecha);
        String titulo = tituloFormat.format(fecha);
        diaNumero.setText(titulo);

        mostrarDialogo("Descargando Indicadores");
        Api.getSharedInstance().obtenerIndicadores(fechaStr, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSIndicadorContainer wrapper = (GSIndicadorContainer)resultado.getObjeto();
                    indicadoresContainer.addAll(wrapper.getListaIndicadores());
                    mAdapter.notifyDataSetChanged();
                } else {
                    Log.e("Api", "Error Al traer Indicadores");
                }
                ocultarDialogo();
            }
        });
    }

    private void obtenerCatalogoIndicadores() {
        Api.getSharedInstance().obtenerTiposIndicadores(new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSTipoIndicadorContainer wrapper = (GSTipoIndicadorContainer) resultado.getObjeto();
                    Integer tmpInt;
                    for (GSTipoIndicador indic : wrapper.getLista()) {
                        tmpInt = Integer.parseInt(indic.id);
                        hashNombres.append(tmpInt, indic.value);
                    }
                }
            }
        });
    }

    @Override
    public void onFechaPressed(Date fecha) {
        String fechaStr = formatter.format(fecha);
        mostrarDialogo("Cargando");
        indicadoresContainer.clear();
        Api.getSharedInstance().obtenerIndicadores(fechaStr, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSIndicadorContainer wrapper = (GSIndicadorContainer)resultado.getObjeto();
                    indicadoresContainer.addAll(wrapper.getListaIndicadores());
                    mAdapter.notifyDataSetChanged();
                } else {
                    Log.e("Api", "Error Al traer Indicadores");
                }
                ocultarDialogo();
            }
        });
    }
}