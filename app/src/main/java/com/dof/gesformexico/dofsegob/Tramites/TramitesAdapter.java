package com.dof.gesformexico.dofsegob.Tramites;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by José Cruz Galindo Martínez on 30/01/17.
 */

public class TramitesAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final String[] mFragmentTitleList = {"Publicación", "Distribución", "Adquisición"};


    public TramitesAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList[position];
    }

    public void agregarFragmento(TramiteFragment fragment) {
        mFragmentList.add(fragment);
    }
}
