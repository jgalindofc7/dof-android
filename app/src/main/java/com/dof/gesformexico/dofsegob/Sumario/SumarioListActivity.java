package com.dof.gesformexico.dofsegob.Sumario;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.ActivityDetalleNota;
import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralActivities.ActividadBase;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNotaContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Sumario.Adapters.SeccionAdapter;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 14/02/17.
 */

public class SumarioListActivity extends ActividadBase implements SeccionAdapter.NotaBySeccionListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumario_details);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        RecyclerView mRecycler = (RecyclerView) findViewById(R.id.activity_sumario_details_list);
        setSupportActionBar(myToolbar);

        String listaTmp = getIntent().getStringExtra(Constants.DESERIALIZAR_LISTA_SECCIONES);
        String titulo = getIntent().getStringExtra(Constants.DESERIALIZAR_TITULO);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(titulo);
        }

        List<GSNota> notas = new LinkedList<>();
        Gson gson = new Gson();
        GSNota[] tmp = gson.fromJson(listaTmp, GSNota[].class);
        notas.addAll(Arrays.asList(tmp));

        SeccionAdapter mAdapter = new SeccionAdapter(this, notas);
        mAdapter.setListener(this);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void mandarNotaPorSeccion(GSNota notaSelected) {
        mostrarDialogo("Cargando Datos");
        Api.getSharedInstance().obtenerDetalleNota(notaSelected.codNota, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSNotaContainer wrapper = (GSNotaContainer)resultado.getObjeto();
                    Intent intent = new Intent(SumarioListActivity.this, ActivityDetalleNota.class);
                    intent.putExtra(Constants.DESERIALIZE_NOTA, wrapper.getNota());
                    startActivity(intent);
                } else {
                    Toast.makeText(SumarioListActivity.this, "No se ha podido recuperar la nota",
                            Toast.LENGTH_SHORT).show();
                }
                ocultarDialogo();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
