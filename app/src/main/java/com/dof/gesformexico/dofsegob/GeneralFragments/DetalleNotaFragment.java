package com.dof.gesformexico.dofsegob.GeneralFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dof.gesformexico.dofsegob.R;

/**
 * @author José Cruz Galindo Martínez on 14/02/17.
 */
public class DetalleNotaFragment extends FragmentoBase {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(0, container, false);
        return root;
    }
}
