package com.dof.gesformexico.dofsegob.Retrofit;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 */

public class ApiError {

    private String response;
    private int messageCode;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }
}
