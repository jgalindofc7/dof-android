package com.dof.gesformexico.dofsegob.Modelo;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 03/01/17.
 */
public class GSTipoIndicadorContainer {

    String response;
    long messageCode;
    private List<GSTipoIndicador> lista;

    public List<GSTipoIndicador> getLista() {
        return lista;
    }

    public void setLista(List<GSTipoIndicador> lista) {
        this.lista = lista;
    }

    @Override
    public String toString() {
        return lista.get(0).toString();
    }


}
