package com.dof.gesformexico.dofsegob.Indicadores.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicador;
import com.dof.gesformexico.dofsegob.R;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 24/04/17.
 */
public class TipoIndicadorAdapter extends ArrayAdapter<GSTipoIndicador> {

    private Context mContext;
    private List<GSTipoIndicador> indicadores;

    public TipoIndicadorAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<GSTipoIndicador> objects) {
        super(context, resource, objects);
        mContext = context;
        indicadores = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.sumario_seccion_item, parent, false);

        TextView txtv = (TextView)view.findViewById(R.id.notas_seccion_titulo);
        txtv.setText(indicadores.get(position).value);
        return view;
    }
}
