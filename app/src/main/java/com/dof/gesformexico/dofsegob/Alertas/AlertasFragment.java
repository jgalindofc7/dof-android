package com.dof.gesformexico.dofsegob.Alertas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Modelo.GSAlerta;
import com.dof.gesformexico.dofsegob.Modelo.GSAlertaContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Utils.Comparadores.AlertasComparador;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Jose Cruz Galindo Martinez
 *         Mexico city 04/04/2017.
 */
public class AlertasFragment extends FragmentoBase {

    // UI
    @BindView(R.id.fragment_alertas_recycler)
    RecyclerView mRecycler;

    // Logic
    private List<GSAlerta> listaAlertas = new LinkedList<>();

    public AlertasFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_alertas, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final AlertasAdapter mAdapter = new AlertasAdapter(getActivity(), listaAlertas);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        Api.getSharedInstance().obtenerAlertasUsuario(new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    listaAlertas.addAll(((GSAlertaContainer)resultado.getObjeto()).alertas);
                    Collections.sort(listaAlertas, new AlertasComparador());
                    mAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(),"Error al obtener alertas", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
