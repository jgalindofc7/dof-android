package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Gersfor on 16/01/17.
 * Clase que representa el modelo que contiene
 * las notas de un sumario.
 */

public class GSNotasContainer {

    @SerializedName("NotasExtraordinarias")
    private List<GSNota> notasExtraodinarias;
    @SerializedName("NotasMatutinas")
    private List<GSNota> notasMatutinas;
    @SerializedName("NotasVespertinas")
    private List<GSNota> notasVespertinas;
    String response;
    int messageCode;

    public List<GSNota> getNotasExtraodinarias(){
        return notasExtraodinarias;
    }

    public void setNotasExtraodinarias(List<GSNota> listaNotasExtra) {
        this.notasExtraodinarias = listaNotasExtra;
    }

    public List<GSNota> getNotasMatutinas(){
        return notasMatutinas;
    }

    public void setNotasMatutinas(List<GSNota> listaNotasMat) {
        this.notasMatutinas = listaNotasMat;
    }

    public List<GSNota> getNotasVespertinas(){
        return notasVespertinas;
    }

    public void setNotasVespertinas(List<GSNota> listaNotasVesp) {
        this.notasVespertinas = listaNotasVesp;
    }

}
