package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author Jose Cruz Galindo Martinez on 04/04/2017.
 */
public class GSCAlertaUsuario {

    @SerializedName("id")
    public long sid;
    @SerializedName("idAlerta")
    public long sidAlerta;
    public String email;
    public String descripcion;
    public String codEstatus;
    public Date fechaFin;
}
