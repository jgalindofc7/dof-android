package com.dof.gesformexico.dofsegob.GeneralFragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;

/**
 * @author José Cruz Galindo Martínez on 09/02/17.
 */

public class FragmentoBase extends Fragment {

    protected ProgressDialog mDialog;

    protected void mostrarDialogo(String mensaje) {
        if (mDialog == null) {
            mDialog = new ProgressDialog(getContext());
            mDialog.setMessage(mensaje);
            mDialog.setCancelable(false);
            mDialog.show();
        } else {
            mDialog.setMessage(mensaje);
        }
    }

    protected void ocultarDialogo() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
