package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

/**
 * @author Jose Cruz Galindo Martinez on 04/04/2017.
 */

public class GSAlerta {

    @SerializedName("id")
    public long sid;
    public String textoBusqueda;
    public GSAlertaUsuario alertaUsuario;


    @Override
    public String toString() {
        return "GSAlerta{" +
                "sid=" + sid +
                ", textoBusqueda='" + textoBusqueda + '\'' +
                ", alertaUsuario=" + alertaUsuario +
                '}';
    }
}
