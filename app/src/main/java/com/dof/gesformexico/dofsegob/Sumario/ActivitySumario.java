package com.dof.gesformexico.dofsegob.Sumario;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.ActivityDetalleNota;
import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNotaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Sumario.Adapters.NotasAdapter;
import com.dof.gesformexico.dofsegob.Utils.Connectivity;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * @author José Cruz Galindo Martínez on 17/03/17.
 */
public class ActivitySumario extends AppCompatActivity implements NotasAdapter.Notas {

    // UI
    @BindView(R.id.actvt_sumario_fecha)
    TextView fechaTxt;
    @BindView(R.id.activity_sumario_fab_descargar)
    FloatingActionButton descargarBtn;
    @BindView(R.id.actvt_sumario_edic_mat)
    Button matutinoBtn;
    @BindView(R.id.actvt_sumario_edic_vesp)
    Button vespertinoBtn;
    @BindView(R.id.actvt_sumario_edic_extra)
    Button extraordinarioBtn;
    @BindView(R.id.actvty_sumario_lista)
    RecyclerView recyclerView;
    private ProgressDialog mDialog;

    // Logic
    private DateFormat format = new SimpleDateFormat("dd-MMMM-yyyy", new Locale("es", "MX"));
    private DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es", "MX"));
    private NotasAdapter mAdapter;
    private Date fechaSelected;
    private List<GSNota> notasM = new LinkedList<>();
    private List<GSNota> notasV = new LinkedList<>();
    private List<GSNota> notasE = new LinkedList<>();
    public static final int MATUTINAS = 0;
    public static final int VESPERTINAS = 1;
    public static final int EXTRAORDINARIAS = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumario);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.actvt_sumario_toolbar);
        setSupportActionBar(myToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ButterKnife.bind(this);

        // Obtener fecha
        long fechaMilis = this.getIntent().getLongExtra(Constants.DESERIALIZAR_FECHA, 0L);
        fechaSelected = new Date(fechaMilis);
        fechaTxt.setText(format.format(fechaSelected));

        // Configuramos la lista
        mAdapter = new NotasAdapter(this, notasM, notasV, notasE);
        mAdapter.setmListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        mDialog = new ProgressDialog(this);

        traerDiarioSeleccionado();
    }


    private void traerDiarioSeleccionado() {
        mDialog.setMessage("Descargando");
        mDialog.setCancelable(false);
        mDialog.show();

        Api.getSharedInstance().obtenerNotas(sdf.format(fechaSelected), new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSNotasContainer wrapper = (GSNotasContainer)resultado.getObjeto();
                    for (GSNota tmp : wrapper.getNotasMatutinas()) {
                        if (tmp.titulo != null){
                            notasM.add(tmp);
                        }
                    }
                    for (GSNota tmp : wrapper.getNotasVespertinas()) {
                        if (tmp.titulo != null){
                            notasV.add(tmp);
                        }
                    }
                    for (GSNota tmp : wrapper.getNotasExtraodinarias()) {
                        if (tmp.titulo != null){
                            notasE.add(tmp);
                        }
                    }
                    configuraBotones();
                }
                if (mDialog.isShowing()){
                    mDialog.dismiss();
                }
            }
        });
    }

    private void configuraBotones() {
        if (notasM.size() > 0) {
            matutinoBtn.setSelected(true);
            mAdapter.setNotasToShow(MATUTINAS);
        } else if (notasV.size() > 0) {
            vespertinoBtn.setSelected(true);
            mAdapter.setNotasToShow(VESPERTINAS);
        }
        if (notasM.size() == 0) {
            matutinoBtn.setVisibility(View.GONE);
        }
        if (notasV.size() == 0) {
            vespertinoBtn.setVisibility(View.GONE);
        }
        if (notasE.size() == 0) {
            extraordinarioBtn.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.activity_sumario_fab_descargar)
    void descargarSumarioCompleto() {
        if (notasM.size() == 0 && notasV.size() == 0
                && notasE.size() == 0) {
            Toast.makeText(this, "No hay Sumario", Toast.LENGTH_SHORT).show();
        } else {
            if (Connectivity.isConnectedWifi(this)) {
                descargarSumario();
            } else {
                DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                descargarSumario();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };


                AlertDialog.Builder alertaB = new AlertDialog.Builder(this);
                alertaB.setMessage("No estas Conectado a una Red WIFI")
                        .setPositiveButton("Descargar", dialogListener)
                        .setNegativeButton("Cancelar", dialogListener);
                alertaB.show();
            }
        }
    }

    private void descargarSumario() {
        String codigoD = "";
        if (matutinoBtn.isSelected()) {
            codigoD = String.valueOf(notasM.get(0).codDiario);
        }
        if (vespertinoBtn.isSelected()) {
            codigoD = String.valueOf(notasV.get(0).codDiario);
        }
        if (extraordinarioBtn.isSelected()) {
            codigoD = String.valueOf(notasE.get(0).codDiario);
        }

        Log.d("ActivitySumario", "Voy a descargar " + codigoD);
        Api.getSharedInstance().descargarSumarioPDF(codigoD, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    File path = Environment.getExternalStorageDirectory();
                    String fechaStr = "Diario-" + sdf.format(fechaSelected) + ".pdf";
                    File file = new File(path, fechaStr);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        ResponseBody resBody = (ResponseBody)resultado.getObjeto();
                        fileOutputStream.write(resBody.bytes());
                        fileOutputStream.close();
                        Toast.makeText(ActivitySumario.this, "El archivo se ha guardado.", Toast.LENGTH_SHORT).show();
                    } catch (IOException ioe) {
                        Log.e("ActivitySumario", ioe.getLocalizedMessage());
                    }
                } else {
                    Log.e("ActivitySumario", "Error al descargar PDF");
                }
            }
        });
    }

    @OnClick(R.id.actvt_sumario_edic_mat)
    void seleccionarMatutinas() {
        matutinoBtn.setSelected(true);
        vespertinoBtn.setSelected(false);
        extraordinarioBtn.setSelected(false);
        mAdapter.setNotasToShow(MATUTINAS);
    }

    @OnClick(R.id.actvt_sumario_edic_vesp)
    void seleccionarVespertinas() {
        vespertinoBtn.setSelected(true);
        matutinoBtn.setSelected(false);
        extraordinarioBtn.setSelected(false);
        mAdapter.setNotasToShow(VESPERTINAS);
    }

    @OnClick(R.id.actvt_sumario_edic_extra)
    void seleccionarExtras() {
        extraordinarioBtn.setSelected(true);
        matutinoBtn.setSelected(false);
        vespertinoBtn.setSelected(false);
        mAdapter.setNotasToShow(EXTRAORDINARIAS);
    }

    @Override
    public void mandarNotaSeleccionada(GSNota notaSelected) {
        mDialog.setMessage("Cargando");
        mDialog.setCancelable(false);
        mDialog.show();
        Api.getSharedInstance().obtenerDetalleNota(notaSelected.codNota, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSNotaContainer wrapper = (GSNotaContainer)resultado.getObjeto();
                    Intent intent = new Intent(ActivitySumario.this, ActivityDetalleNota.class);
                    intent.putExtra(Constants.DESERIALIZE_NOTA, wrapper.getNota());
                    startActivity(intent);
                } else {
                    Toast.makeText(ActivitySumario.this, "Error al traer Nota", Toast.LENGTH_SHORT)
                            .show();
                }
                if (mDialog.isShowing()){
                    mDialog.dismiss();
                }
            }
        });
    }
}
