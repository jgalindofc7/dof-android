package com.dof.gesformexico.dofsegob.Modelo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Comparator;

/**
 * @author josé Cruz Galindo Martínez on 17/01/17.
 */

public class GSTiposTopicos extends BaseModel {

    public  String id;
    public  String value;
}
