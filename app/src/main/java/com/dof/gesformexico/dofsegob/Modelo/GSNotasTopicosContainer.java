package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Gersfor on 18/01/17.
 */

public class GSNotasTopicosContainer {

    @SerializedName("NotasTopicos")
    private List<GSNotasTopicos> notasTopicos;
    private int messageCode;
    private String response;

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<GSNotasTopicos> getNotasTopicosContainer() {
        return notasTopicos;
    }

    public void setNotasTopicos(List<GSNotasTopicos> listaNotasTopicos) {
        this.notasTopicos = listaNotasTopicos;
    }

}
