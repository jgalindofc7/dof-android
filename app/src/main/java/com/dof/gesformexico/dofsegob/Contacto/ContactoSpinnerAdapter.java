package com.dof.gesformexico.dofsegob.Contacto;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSCatalogoContacto;
import com.dof.gesformexico.dofsegob.R;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 04/05/17.
 */
public class ContactoSpinnerAdapter extends ArrayAdapter<String> {

    private List<String> temporal;
    private LayoutInflater inflater;

    public ContactoSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        Context mContext = context;
        temporal = objects;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        View view = inflater.inflate(R.layout.sumario_seccion_item, parent, false);

        TextView txtv = (TextView)view.findViewById(R.id.notas_seccion_titulo);
        txtv.setText(temporal.get(position));
        return view;
    }
}
