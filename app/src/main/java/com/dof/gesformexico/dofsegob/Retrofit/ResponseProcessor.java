package com.dof.gesformexico.dofsegob.Retrofit;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * @author José Cruz Galindo Martínez on 29/12/16.
 */

public class ResponseProcessor<E> {

    private final Retrofit retrofit;
    private final TaskCallback taskCallback;

    public interface InternalProcessor<E> {
        E onResponse(Response<E> response) throws Exception;
    }

    private InternalProcessor<E> internalProcessor = null;

    public ResponseProcessor(Retrofit retro, TaskCallback callback, InternalProcessor internalProc) {
        this.retrofit = retro;
        this.taskCallback = callback;
        this.internalProcessor = internalProc;
    }

    public ResponseProcessor(Retrofit retro, TaskCallback callback) {
        this(retro, callback, null);
    }

    public Callback<E> createCallback() {
        return new Callback<E>() {
            @Override
            public void onResponse(Call<E> call, Response<E> response) {
                if (response.isSuccessful()) {
                    E objetos = null;
                    if (internalProcessor != null) {
                        try {
                            objetos = internalProcessor.onResponse(response);
                        } catch (Exception e) {
                            enviarError(e);
                        }
                    }
                    final ResultadoApi<E> resultadoApi = new ResultadoApi<>();
                    resultadoApi.setExito(true);
                    resultadoApi.setObjeto(objetos);

                    if (taskCallback != null) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                taskCallback.processResult(resultadoApi);
                            }
                        });
                    } else if (response.errorBody() != null) {
                        procesarError(response, retrofit);
                    }
                } else {
                    Log.e("Retrofit", "response is not successful");
                    ApiError apiError = new ApiError();
                    apiError.setMessageCode(response.code());
                    try {
                        apiError.setResponse(response.errorBody().string());
                        enviarError(apiError);
                    } catch (IOException ioe) {
                        enviarError(ioe);
                    }
                }
            }

            @Override
            public void onFailure(Call<E> call, Throwable t) {
                enviarError(t);
            }
        };
    }

    private void procesarError(Response<E> response, Retrofit retrofit) {
        Converter<ResponseBody, ApiError> errorConverter = retrofit.responseBodyConverter(ApiError.class, new Annotation[0]);
        try {
            ApiError apiError = errorConverter.convert(response.errorBody());
            if (apiError == null) {
                apiError = new ApiError();
                apiError.setMessageCode(404);
                apiError.setResponse(response.errorBody().string());
                Log.e("Retrofit",response.errorBody().string());
            }
            enviarError(apiError);
        }catch (Exception e) {
            enviarError(e);
        }
    }

    private void enviarError(Throwable throwable) {
        ApiError apiError = new ApiError();
        if (throwable instanceof IOException) {
            apiError.setResponse("Error de red");
        } else {
            apiError.setResponse(throwable.getMessage());
            apiError.setMessageCode(404);
            Log.e("Retrofit",throwable.getMessage());
        }
        enviarError(apiError);
    }

    private void enviarError(ApiError apiE) {
        if (taskCallback != null && apiE != null) {
            final ResultadoApi<E>  resultadoApi = new ResultadoApi<>();
            resultadoApi.setExito(false);
            resultadoApi.setMotivo(apiE.getResponse());

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    taskCallback.processResult(resultadoApi);
                }
            });
        }
    }

}
