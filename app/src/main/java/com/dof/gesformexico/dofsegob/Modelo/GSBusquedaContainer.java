package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 22/03/17.
 */

public class GSBusquedaContainer {

    private int messageCode;
    private String response;
    @SerializedName("Notas")
    private List<GSNota> notas;

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<GSNota> getNotas() {
        return notas;
    }

    public void setNotas(List<GSNota> notas) {
        this.notas = notas;
    }
}
