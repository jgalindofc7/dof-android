package com.dof.gesformexico.dofsegob.Sumario.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Sumario.ActivitySumario;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author José Cruz Galindo Martínez on 18/04/17.
 */
public class NotasAdapter extends RecyclerView.Adapter<NotasAdapter.NotasVH> {

    public interface Notas {
        void mandarNotaSeleccionada(GSNota notaSelected);
    }

    private List<GSNota> notasMatutinas;
    private List<GSNota> notasVespertinas;
    private List<GSNota> notasExtra;
    private List<GSNota> notasVisibles = new LinkedList<>();
    private LayoutInflater mInflater;
    private DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es", "MX"));
    private Notas mListener;

    public NotasAdapter(Activity activity, List<GSNota> ntsM, List<GSNota> ntsV, List<GSNota> ntsE){
        notasMatutinas = ntsM;
        notasVespertinas = ntsV;
        notasExtra = ntsE;
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public NotasVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = mInflater.inflate(R.layout.notas_item, parent, false);
        return new NotasVH(vista);
    }

    @Override
    public void onBindViewHolder(NotasVH holder, int position) {
        final GSNota slctd = notasVisibles.get(position);
        holder.titulo.setText(slctd.titulo);
        String orga = (slctd.codOrgaDos != null)? slctd.codOrgaDos : slctd.codOrgaUno;
        holder.dependencia.setText(orga);
        holder.fecha.setText(sdf.format(slctd.fecha));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.mandarNotaSeleccionada(slctd);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notasVisibles.size();
    }

    public void setNotasToShow(int notasToShow) {
        switch (notasToShow) {
            case ActivitySumario.MATUTINAS:
                notasVisibles = notasMatutinas;
                break;
            case ActivitySumario.VESPERTINAS:
                notasVisibles = notasVespertinas;
                break;
            case ActivitySumario.EXTRAORDINARIAS:
                notasVisibles = notasExtra;
                break;
        }
        this.notifyDataSetChanged();
    }

    public void setmListener(Notas mListener) {
        this.mListener = mListener;
    }

    class NotasVH extends RecyclerView.ViewHolder {

        @BindView(R.id.notas_item_titulo)
        TextView titulo;
        @BindView(R.id.notas_item_dependencia)
        TextView dependencia;
        @BindView(R.id.notas_item_fecha)
        TextView fecha;

        NotasVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
