package com.dof.gesformexico.dofsegob.Indicadores;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.dof.gesformexico.dofsegob.Indicadores.Adapter.SimpleFragmentPagerAdapter;
import com.dof.gesformexico.dofsegob.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Jose Cruz Galindo Martinez
 * Francisco Javier  Flores Morales
 * fflores.mo@gesfor.com.mx
 */
public class IndicadoresActivity extends AppCompatActivity {

    @BindView(R.id.coti_toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.activity_cotizador_view_pager)
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cotizador);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        configurarTabs();
        tabLayout.setupWithViewPager(viewPager);
    }

    private void configurarTabs() {
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}