package com.dof.gesformexico.dofsegob.Api;

import android.support.compat.BuildConfig;
import android.util.Log;

import com.dof.gesformexico.dofsegob.Modelo.GSAlertaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSBusquedaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCAlertaUsuario;
import com.dof.gesformexico.dofsegob.Modelo.GSCatalogoContactoContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCotizacionContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCotizador;
import com.dof.gesformexico.dofsegob.Modelo.GSEnvioContacto;
import com.dof.gesformexico.dofsegob.Modelo.GSEnvioQuejas;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicadorContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasTopicosContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicador;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicadorContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTopicosContainer;
import com.dof.gesformexico.dofsegob.Retrofit.MyGsonBuilder;
import com.dof.gesformexico.dofsegob.Retrofit.ResponseProcessor;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 * Clase que contiene los metodos para hacer consumo de servicios rest.
 *
 */
public class Api {

    private Retrofit retrofit;
    private DofService dofService;
    private static Api sharedInstance;
    private String tokenMail;

    private Api() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addNetworkInterceptor(logging);
            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(
                        MyGsonBuilder.getGsonBuilder().create()))
                .client(builder.build())
                .build();
        dofService = retrofit.create(DofService.class);
    }

    public static Api getSharedInstance() {
        if (sharedInstance == null) {
            sharedInstance = new Api();
            sharedInstance.tokenMail = "hector@dof.com";
        }
        return sharedInstance;
    }


    /**
     * Obtenemos los indicadores en la fecha seleccionada
     * @param fecha La Fecha de la cual vamos a obtener los indicadores con
     *              el formato dd-MM-yyyy.
     * @param callback Las Acciones que se haran cuando termine el metodo.
     */
    public void obtenerIndicadores(final String fecha, TaskCallback callback) {
        Call<GSIndicadorContainer> request = dofService.getIndicadores(fecha);
        request.enqueue(new ResponseProcessor<GSIndicadorContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSIndicadorContainer>() {
            @Override
            public GSIndicadorContainer onResponse(Response<GSIndicadorContainer> response) throws Exception {
                // Si lo necesito lo guardo aqui
                return response.body();
            }
        }).createCallback());
    }

    public void obtenerTiposIndicadores(TaskCallback callback) {
        Call<GSTipoIndicadorContainer> request = dofService.getTipoIndicadores();
        request.enqueue(new ResponseProcessor<GSTipoIndicadorContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSTipoIndicadorContainer>() {
            @Override
            public GSTipoIndicadorContainer onResponse(Response<GSTipoIndicadorContainer> response) throws Exception {
                List<GSTipoIndicador> container = response.body().getLista();
                for (GSTipoIndicador tipoInd: container) {
                    tipoInd.save();
                }
                Log.e("Prueba", response.body().toString());
                return response.body();
            }
        }).createCallback());
    }

    public void obtenerIndicadoresPorRango(final String TipoIndicador, final String fechaInicio,
                                            final String fechaFin, TaskCallback callback) {
        Call<GSIndicadorContainer> request = dofService.getIndicadoresRango(TipoIndicador, fechaInicio, fechaFin);
        request.enqueue(new ResponseProcessor<GSIndicadorContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSIndicadorContainer>() {
            @Override
            public GSIndicadorContainer onResponse(Response<GSIndicadorContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }
    public void obtenerNotas(final String fecha, TaskCallback callback){
        Call<GSNotasContainer> request = dofService.getNotaContainer(fecha);
        request.enqueue(new ResponseProcessor<GSNotasContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSNotasContainer>() {
            @Override
            public GSNotasContainer onResponse(Response<GSNotasContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void catalogoTopicos(TaskCallback callback){
        Call<GSTopicosContainer> request = dofService.getCatTopicos();
        request.enqueue(new ResponseProcessor<GSTopicosContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSTopicosContainer>() {
            @Override
            public GSTopicosContainer onResponse(Response<GSTopicosContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void obtenerNotasTopicos(final String idTopico,
                                     final String pagina,
                                    final String sorting,
                                     final String orden,
                                     TaskCallback callback){
        Call<GSNotasTopicosContainer> request = dofService.getNotasTopicos(idTopico, pagina, sorting, orden);
        request.enqueue(new ResponseProcessor<GSNotasTopicosContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSNotasTopicosContainer>() {
            @Override
            public GSNotasTopicosContainer onResponse(Response<GSNotasTopicosContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void obtenerDetalleNota(final long idNota, TaskCallback callback) {
        Call<GSNotaContainer> request = dofService.getDetalleNota(idNota);
        request.enqueue(new ResponseProcessor<GSNotaContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSNotaContainer>() {
            @Override
            public GSNotaContainer onResponse(Response<GSNotaContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void mandarArchivoCotizacion(final GSCotizador cotizador, TaskCallback callback) {
        Call<GSCotizacionContainer> request = dofService.mandarCotizacion(cotizador);
        request.enqueue(new ResponseProcessor<GSCotizacionContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSCotizacionContainer>() {
            @Override
            public GSCotizacionContainer onResponse(Response<GSCotizacionContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void mandarBusquedaAvanzada(final String titulo, final long fechaI,
                                       final long fechaF, TaskCallback callback) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es","MX"));
        String tituloWeb = titulo;
        try {
            tituloWeb = URLEncoder.encode(titulo, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            Log.e("API", "UTF-8 Not Supported");
        }
        Call<GSBusquedaContainer> request = dofService.realizarBusquedaAvanzada(tituloWeb,
                sdf.format(new Date(fechaI)), sdf.format(new Date(fechaF)));
        request.enqueue(new ResponseProcessor<GSBusquedaContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSBusquedaContainer>() {
            @Override
            public GSBusquedaContainer onResponse(Response<GSBusquedaContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void obtenerAlertasUsuario(TaskCallback callback) {
        Call<GSAlertaContainer> request = dofService.obtenerAlertasUsuario(tokenMail);
        request.enqueue(new ResponseProcessor<GSAlertaContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSAlertaContainer>() {
            @Override
            public GSAlertaContainer onResponse(Response<GSAlertaContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void suscribirAlertaUsuario(GSCAlertaUsuario alerta, TaskCallback callback) {
        Call<GSAlertaContainer> request = dofService.suscribirAlerta(alerta);
        alerta.email = this.tokenMail;
        request.enqueue(new ResponseProcessor<GSAlertaContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSAlertaContainer>() {
            @Override
            public GSAlertaContainer onResponse(Response<GSAlertaContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void actualizarAlerta(final GSCAlertaUsuario alerta, TaskCallback callback) {
        Call<GSAlertaContainer> request = dofService.renovarAlerta(alerta);
        request.enqueue(new ResponseProcessor<GSAlertaContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSAlertaContainer>() {
            @Override
            public GSAlertaContainer onResponse(Response<GSAlertaContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void descargarSumarioPDF(final String codDiario, TaskCallback callback) {
        Call<ResponseBody> request = dofService.descargarPDF(codDiario);
        request.enqueue(new ResponseProcessor<ResponseBody>(retrofit, callback, new ResponseProcessor.InternalProcessor<ResponseBody>() {
            @Override
            public ResponseBody onResponse(Response<ResponseBody> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void descargarDocumentoDoc(final String codNota, TaskCallback callback) {
        Call<ResponseBody> request = dofService.descargarDoc(codNota);
        request.enqueue(new ResponseProcessor<ResponseBody>(retrofit, callback, new ResponseProcessor.InternalProcessor<ResponseBody>() {
            @Override
            public ResponseBody onResponse(Response<ResponseBody> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void descargarCatalogoContacto(TaskCallback callback) {
        Call<GSCatalogoContactoContainer> request = dofService.obtenerCatalogoContacto();
        request.enqueue(new ResponseProcessor<GSCatalogoContactoContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSCatalogoContactoContainer>() {
            @Override
            public GSCatalogoContactoContainer onResponse(Response<GSCatalogoContactoContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }

    public void descargarCatalogoAreas(TaskCallback callback) {
        Call<GSCatalogoContactoContainer> request = dofService.obtenerCatalogoAreas();
        request.enqueue(new ResponseProcessor<GSCatalogoContactoContainer>(retrofit, callback, new ResponseProcessor.InternalProcessor<GSCatalogoContactoContainer>() {
            @Override
            public GSCatalogoContactoContainer onResponse(Response<GSCatalogoContactoContainer> response) throws Exception {
                return response.body();
            }
        }).createCallback());
    }


    public void mandarContacto(final GSEnvioContacto envioContacto, TaskCallback callback) {
        Call<ResponseBody> request = dofService.mandarContacto(envioContacto);

    }

    public void mandarQueja(final GSEnvioQuejas envioQueja, TaskCallback callback) {
        Call<ResponseBody> request = dofService.mandarSugerencia(envioQueja);
    }

}
