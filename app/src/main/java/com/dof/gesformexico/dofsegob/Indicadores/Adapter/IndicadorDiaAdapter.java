package com.dof.gesformexico.dofsegob.Indicadores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Indicadores.IndicadorSelectedListener;
import com.dof.gesformexico.dofsegob.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author José Cruz Galindo Martínez on 17/02/17.
 */
public class IndicadorDiaAdapter extends RecyclerView.Adapter<IndicadorDiaAdapter.IndicadorDiaViewHolder> {

    private SimpleDateFormat formatter;
    private LayoutInflater inflater;
    private Date[] semana;
    private Boolean[] seleccionado;
    private int previosSelected;
    public IndicadorSelectedListener mListener;

    public IndicadorDiaAdapter(Activity activity) {
        Date hoy = new Date();
        formatter = new SimpleDateFormat("EEEE", new Locale("es", "MX"));
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(hoy);
        int NUMERO_DIAS = 7;
        semana = new Date[NUMERO_DIAS];
        seleccionado = new Boolean[NUMERO_DIAS];
        semana[6] = hoy;
        previosSelected = 6;
        for (int i = NUMERO_DIAS - 2; i >= 0; i--) {
            calendario.add(Calendar.DAY_OF_MONTH, -1);
            semana[i] = calendario.getTime();
        }
    }

    @Override
    public IndicadorDiaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = inflater.inflate(R.layout.indicador_dia_item, parent, false);
        return new IndicadorDiaViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(IndicadorDiaViewHolder holder, int position) {
        final Date fecha = semana[position];
        final int posicion = position;
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        holder.nombre.setText(formatter.format(fecha));
        int fondo = posicion == previosSelected ? R.drawable.fondo_redondo_active
                : R.drawable.fondo_redondo;
        holder.backg.setBackgroundResource(fondo);
        holder.dia.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionado[previosSelected] = false;
                previosSelected = posicion;
                seleccionado[posicion] = true;
                notifyDataSetChanged();
                mListener.onFechaPressed(fecha);
            }
        });
    }

    @Override
    public int getItemCount() {
        return semana.length;
    }

    class IndicadorDiaViewHolder extends RecyclerView.ViewHolder {

        View backg;
        TextView nombre;
        TextView dia;

        IndicadorDiaViewHolder(View itemView) {
            super(itemView);

            nombre = (TextView) itemView.findViewById(R.id.indicador_dia_item_nombre);
            dia = (TextView) itemView.findViewById(R.id.indicador_dia_item_numero);
            backg = itemView.findViewById(R.id.indicador_dia_item_background);
        }
    }
}
