package com.dof.gesformexico.dofsegob.Topicos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dof.gesformexico.dofsegob.R;

/**
 * @author José Cruz Galindo Martínez on 21/03/17.
 */

public class TopicosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topicos);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.actvt_topicos_toolbar);
        setSupportActionBar(myToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new TiposTopicosFragment();
        fm.replace(R.id.actvt_topicos_container, fragment).commit();
    }
}
