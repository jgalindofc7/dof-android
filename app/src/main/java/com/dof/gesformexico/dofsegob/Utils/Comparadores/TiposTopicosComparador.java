package com.dof.gesformexico.dofsegob.Utils.Comparadores;

import com.dof.gesformexico.dofsegob.Modelo.GSTiposTopicos;

import java.util.Comparator;

/**
 * @author José Cruz Galindo Martínez on 15/02/17.
 */

public class TiposTopicosComparador implements Comparator<GSTiposTopicos> {


    @Override
    public int compare(GSTiposTopicos t1, GSTiposTopicos t2) {
        return t1.id.compareTo(t2.id);
    }
}
