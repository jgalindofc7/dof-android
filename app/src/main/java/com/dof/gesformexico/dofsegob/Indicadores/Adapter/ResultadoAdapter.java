package com.dof.gesformexico.dofsegob.Indicadores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSIndicador;
import com.dof.gesformexico.dofsegob.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * @author José Cruz Galindo Martínez on 25/04/17.
 */
public class ResultadoAdapter extends RecyclerView.Adapter<ResultadoAdapter.IndicadorResultVH> {

    private LayoutInflater inflater;
    private List<GSIndicador> indicadores;
    private DateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy", new Locale("es", "MX"));

    public ResultadoAdapter(Activity activity, List<GSIndicador> indicadors){
        indicadores = indicadors;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public IndicadorResultVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = inflater.inflate(R.layout.indicador_item, parent, false);
        return new IndicadorResultVH(vista);
    }

    @Override
    public void onBindViewHolder(IndicadorResultVH holder, int position) {
        GSIndicador slctd = indicadores.get(position);
        holder.fecha.setText(sdf.format(slctd.getFecha()));
        holder.valor.setText(slctd.getValor());
    }

    @Override
    public int getItemCount() {
        return indicadores.size();
    }

    class IndicadorResultVH extends RecyclerView.ViewHolder {

        TextView fecha;
        TextView valor;

        IndicadorResultVH(View itemView) {
            super(itemView);
            fecha = (TextView)itemView.findViewById(R.id.indicador_item_nombre);
            valor = (TextView)itemView.findViewById(R.id.indicador_item_valor);
        }
    }
}
