package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Jose Cruz Galindo Martinez on 04/04/2017.
 */

public class GSAlertaContainer {

    public int messageCode;
    @SerializedName("response")
    public String respuesta;
    public List<GSAlerta> alertas;
}
