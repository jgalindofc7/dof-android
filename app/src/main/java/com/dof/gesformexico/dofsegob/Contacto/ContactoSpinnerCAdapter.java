package com.dof.gesformexico.dofsegob.Contacto;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSCatalogoContacto;
import com.dof.gesformexico.dofsegob.R;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 04/05/17.
 */
public class ContactoSpinnerCAdapter extends ArrayAdapter<GSCatalogoContacto> {

    private List<GSCatalogoContacto> contactos;
    private LayoutInflater inflater;

    public ContactoSpinnerCAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<GSCatalogoContacto> objects) {
        super(context, resource, objects);
        Context mContext = context;
        contactos = objects;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    private View getCustomView(int position, ViewGroup parent) {
        View view = inflater.inflate(R.layout.sumario_seccion_item, parent, false);

        TextView txtv = (TextView)view.findViewById(R.id.notas_seccion_titulo);
        txtv.setText(contactos.get(position).value);
        return view;
    }
}
