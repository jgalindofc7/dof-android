package com.dof.gesformexico.dofsegob.Topicos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasTopicosContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTiposTopicos;
import com.dof.gesformexico.dofsegob.Modelo.GSTopicosContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Topicos.AdapterTopicos.TiposTopicosAdapter;
import com.dof.gesformexico.dofsegob.Utils.Comparadores.TiposTopicosComparador;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.google.gson.Gson;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author José Cruz Galindo Martínez on 21/03/17.
 */
public class TiposTopicosFragment extends FragmentoBase implements
        TiposTopicosAdapter.TiposTopicosListener {

    // UI
    @BindView(R.id.content_topicos_recycler_view)
    RecyclerView listaTopicos;

    // Logic
    private List<GSTiposTopicos> tiposTopicosList;
    private TiposTopicosAdapter mAdapter;

    public TiposTopicosFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_topicos, container, false);
        ButterKnife.bind(this, v);

        tiposTopicosList = new LinkedList<>();
        mAdapter = new TiposTopicosAdapter(getActivity(), tiposTopicosList);
        mAdapter.setListener(this);
        listaTopicos.setLayoutManager(new LinearLayoutManager(getActivity()));
        listaTopicos.setAdapter(mAdapter);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Api.getSharedInstance().catalogoTopicos(new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()){
                    GSTopicosContainer wrapper = (GSTopicosContainer)resultado.getObjeto();
                    //TiposTopicosComparador comparador = new TiposTopicosComparador();
                    //List<GSTiposTopicos> lista = wrapper.getListaTopicos();
                    //Collections.sort(lista, comparador);
                    tiposTopicosList.addAll(wrapper.getListaTopicos());
                    mAdapter.notifyDataSetChanged();
                } else {
                    Log.e("TiposTopicosFragment", "No fue Exitoso");
                }
            }
        });
    }

    @Override
    public void descargarTopicos(String idTT) {
        mostrarDialogo("Descargando Topicos");
        Api.getSharedInstance().obtenerNotasTopicos(idTT, "1", "fecha", "DESC", new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSNotasTopicosContainer wrapper = (GSNotasTopicosContainer) resultado.getObjeto();
                    String lista = new Gson().toJson(wrapper.getNotasTopicosContainer());
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DESERIALIZAR_NOTAS_TOPICOS, lista);
                    TopicosFragment fragment =  new TopicosFragment();
                    fragment.setArguments(bundle);

                    getFragmentManager().beginTransaction()
                            .add(R.id.actvt_topicos_container, fragment)
                            .commit();
                } else {
                    Log.e("Topicos Fragment", "No traje Topicos");
                }
                ocultarDialogo();
            }
        });
    }
}
