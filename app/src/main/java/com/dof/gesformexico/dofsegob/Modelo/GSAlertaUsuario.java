package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author Jose Cruz Galindo Martinez on 04/04/2017.
 */

public class GSAlertaUsuario {

    @SerializedName("id")
    public long sid;
    @SerializedName("idAlerta")
    public long sidAlerta;
    public String email;
    public String descripcion;
    public int codEstatus;
    public Date fechaIni;
    public Date fechaFin;

    @Override
    public String toString() {
        return "GSAlertaUsuario{" +
                "sid=" + sid +
                ", sidAlerta=" + sidAlerta +
                ", email='" + email + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", codEstatus=" + codEstatus +
                ", fechaIni=" + fechaIni +
                ", fechaFin=" + fechaFin +
                '}';
    }
}
