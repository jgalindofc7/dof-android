package com.dof.gesformexico.dofsegob.Tramites;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.R;
import com.github.barteksc.pdfviewer.PDFView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author José Cruz Galindo Martínez on 30/01/17.
 */

public class TramiteFragment extends Fragment {

    @BindView(R.id.fragment_tramites_pdfviewer)
    PDFView pdfViewer;
    @BindView(R.id.fragment_tramites_titulo)
    TextView tituloSecc;
    @BindView(R.id.fragment_tramites_radio_group)
    RadioGroup grupoBotones;

    private final String[] PDF_FILES = {"publicacion.pdf",
            "distribucion.pdf", "adquisicion.pdf"};

    public TramiteFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tramite, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        grupoBotones.check(R.id.fragment_tramites_rbtn_publicar);
        grupoBotones.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.fragment_tramites_rbtn_publicar:
                        tituloSecc.setText(R.string.tram_publicar_com);
                        cambiarPDF(0);
                        break;
                    case R.id.fragment_tramites_rbtn_distribuir:
                        tituloSecc.setText(R.string.tram_distri_com);
                        cambiarPDF(1);
                        break;
                    case R.id.fragment_tramites_rbtn_adquirir:
                        tituloSecc.setText(R.string.tram_adquiri_com);
                        cambiarPDF(2);
                        break;
                }
            }
        });

        this.cambiarPDF(0);
    }

    private void cambiarPDF(int posicion) {
        pdfViewer.fromAsset(PDF_FILES[posicion]).load();
    }
}
