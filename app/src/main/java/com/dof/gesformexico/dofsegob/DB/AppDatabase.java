package com.dof.gesformexico.dofsegob.DB;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * @author José Cruz Galindo Martínez on 03/01/17.
 */

@Database(name = AppDatabase.DATABASE_NAME, version = AppDatabase.VERSION)
public class AppDatabase {

    static final String DATABASE_NAME = "DOFDB";
    static final int VERSION = 1;
}
