package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Gersfor on 17/01/17.
 */

public class GSTopicosContainer {
    private String response;


    @SerializedName("lista")
    private List<GSTiposTopicos> lista;



    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    private int messageCode;

    public List<GSTiposTopicos> getListaTopicos() {
        return lista;
    }

    public void setListaTopicoslista(List<GSTiposTopicos> lista) {
        this.lista = lista;
    }
}
