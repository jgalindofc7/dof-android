package com.dof.gesformexico.dofsegob.Indicadores.Fragment;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Indicadores.Adapter.TipoIndicadorAdapter;
import com.dof.gesformexico.dofsegob.Indicadores.ResultadoIndicadoresActivity;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicador;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicadorContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicador;
import com.dof.gesformexico.dofsegob.Modelo.GSTipoIndicadorContainer;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.dof.gesformexico.dofsegob.fecha.CustomDatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Jose Cruz Galindo Martinez
 */
public class SearchFragment extends FragmentoBase implements DatePickerDialog.OnDateSetListener {

    // UI
    @BindView(R.id.fragment_search_fecha_inicio_btn)
    Button fInicioBtn;
    @BindView(R.id.fragment_search_fecha_fin_btn)
    Button fFinBtn;
    @BindView(R.id.fragment_search_spinner)
    Spinner indicadorSpinner;
    @BindView(R.id.fragment_search_buscar)
    Button buscar;

    // Logic
    private DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es","MX"));
    private GSTipoIndicador indicadorSelected;
    private long fechaInicial = -1;
    private long fechaFinal = -1;
    private int dialogType = 0;
    private List<GSTipoIndicador> tiposIndicadores;

    public SearchFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Aqui el adapter
        tiposIndicadores = new LinkedList<>();
        final TipoIndicadorAdapter mAdapter = new TipoIndicadorAdapter(getContext(),
                android.R.layout.simple_dropdown_item_1line,
                tiposIndicadores);
        indicadorSpinner.setAdapter(mAdapter);
        indicadorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                indicadorSelected = tiposIndicadores.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Api.getSharedInstance().obtenerTiposIndicadores(new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSTipoIndicadorContainer wrapper = (GSTipoIndicadorContainer) resultado.getObjeto();
                    tiposIndicadores.addAll(wrapper.getLista());
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @OnClick(R.id.fragment_search_fecha_inicio_btn)
    void mostrarCalendarFechaInicial() {
        dialogType = 7;
        mostrarCalendario();
    }

    @OnClick(R.id.fragment_search_fecha_fin_btn)
    void  mostrarCalendarioFechaFinal() {
        dialogType = 17;
        mostrarCalendario();
    }

    @OnClick(R.id.fragment_search_buscar)
    void buscarIndicadores() {
        Date fechaI = new Date(fechaInicial);
        Date fechaF = new Date(fechaFinal);

        if (datosValidos()) {
            mostrarDialogo("Descargando");
            Api.getSharedInstance().obtenerIndicadoresPorRango(indicadorSelected.id, sdf.format(fechaI),
                    sdf.format(fechaF), new TaskCallback() {
                        @Override
                        public void processResult(ResultadoApi resultado) {
                            if (resultado.isExito()){
                                List<GSIndicador> tt = ((GSIndicadorContainer) resultado.getObjeto()).getListaIndicadores();
                                GSIndicador[] array = new GSIndicador[1];
                                Intent rIntent = new Intent(getContext(), ResultadoIndicadoresActivity.class);
                                rIntent.putExtra(Constants.DESERIALIZAR_INDICADORES, tt.toArray(array));
                                rIntent.putExtra(Constants.DESERIALIZAR_INDICADOR_BUSQ, indicadorSelected.value);
                                startActivity(rIntent);
                            }
                            ocultarDialogo();
                        }
                    });
        }
    }

    private boolean datosValidos() {
        boolean valido = true;
        String mensaje = "";
        if (indicadorSelected == null) {
            valido = false;
            mensaje = "Indicador no valido.";
        }
        if (fechaInicial < 0) {
            valido = false;
            mensaje = "Fecha inicial no valida.";
        }
        if (fechaFinal < 0) {
            valido = false;
            mensaje = "Fecha final no valida.";
        }
        if (!valido) {
            Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        }
        return valido;
    }

    private void mostrarCalendario() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mostrarCalendarioForN();
        } else {
            mostrarCalendarioForL();
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void mostrarCalendarioForN() {
        DatePickerDialog datePicker = new DatePickerDialog(getContext());
        datePicker.setOnDateSetListener(this);
        datePicker.show();
    }

    private void mostrarCalendarioForL() {
        CustomDatePicker datePickerF = new CustomDatePicker();
        datePickerF.setListener(this);
        datePickerF.show(getActivity().getFragmentManager(), "Seleccionar Fecha");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);
        if (dialogType == 7) {
            fInicioBtn.setText(sdf.format(c.getTime()));
            fechaInicial = c.getTimeInMillis();
        } else {
            fFinBtn.setText(sdf.format(c.getTime()));
            fechaFinal = c.getTimeInMillis();
        }
    }
}