package com.dof.gesformexico.dofsegob.Splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.dof.gesformexico.dofsegob.MainActivity;
import com.dof.gesformexico.dofsegob.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends Activity {

    private static final long TIME_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        final Intent mainIntent = new Intent(this, MainActivity.class);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                startActivity(mainIntent);
                finish();
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, TIME_DELAY);
    }

}
