package com.dof.gesformexico.dofsegob.Favoritos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.R;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author José Cruz Galindo Martínez on 03/02/17.
 */
public class FavoritosFragment extends Fragment {

    @BindView(R.id.fragment_favorito_list)
    RecyclerView mRecycler;

    // Logic
    private FavoritosAdapter mAdapter;
    private List<GSNota> notasFavoritas;

    public FavoritosFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notasFavoritas = new LinkedList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favoritos, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new FavoritosAdapter(getActivity(), notasFavoritas);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        notasFavoritas.clear();
        notasFavoritas.addAll(SQLite.select().from(GSNota.class).queryList());
        mAdapter.notifyDataSetChanged();
        Log.d("FavoritosFragment", "favoritos " + notasFavoritas.size());
    }
}
