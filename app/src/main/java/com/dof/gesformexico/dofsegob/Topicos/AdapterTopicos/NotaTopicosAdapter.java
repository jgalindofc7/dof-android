package com.dof.gesformexico.dofsegob.Topicos.AdapterTopicos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.ActivityDetalleNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasTopicos;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;


/**
 *  @author José Cruz Galindo Martínez on 07/02/17.
 */
public class NotaTopicosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Activity mActivity;
    private LayoutInflater inflater;
    private List<GSNotasTopicos> notas;
    private int paginaActual = 1;
    private boolean ascendente = true;
    private static final String ASCENDING = "ASC";
    private static final String DESCENDING = "DESC";
    private SimpleDateFormat dateFormat;

    private NotaByTopicoListener mListener;

    public NotaTopicosAdapter(Activity activity, List<GSNotasTopicos> listaUtilizada) {
        mActivity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        notas = listaUtilizada;
        dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = inflater.inflate(R.layout.topicos_item, parent, false);
        return new NotasAdapterViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final GSNotasTopicos selected = notas.get(position);
        NotasAdapterViewHolder v = (NotasAdapterViewHolder)holder;
        v.titulo.setText(selected.getTitulo());
        v.dependencia.setText(selected.getCodOrgaDos());
        v.fecha.setText(dateFormat.format(selected.getFecha()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.descargarNota(selected.getCodNota());
//                Api.getSharedInstance().obtenerDetalleNota(selected.getCodNota(), new TaskCallback() {
//                    @Override
//                    public void processResult(ResultadoApi resultado) {
//                        Log.d("NotasTopicosAdapter", "Dercargar nota " + selected.getCodNota());
//                        if (resultado.isExito()) {
//                            GSNotaContainer wrapper = (GSNotaContainer) resultado.getObjeto();
//                            mostrarDetalleNota(wrapper.getNota());
//                        } else {
//                            Log.e("NotasTopicosAdapter", "No se Descargo la nota");
//                        }
//                    }
//                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return notas.size();
    }

    public void setListener(NotaByTopicoListener listener) {
        this.mListener = listener;
    }

    private void descargarNotas(String idTopico) {
    }

    private void mostrarDetalleNota(GSNota nota) {
        Intent detailNotaIntent = new Intent(mActivity, ActivityDetalleNota.class);
        detailNotaIntent.putExtra(Constants.DESERIALIZE_NOTA, new Gson().toJson(nota));
        mActivity.startActivity(detailNotaIntent);
    }

    /**
     * View Holder Para los contenedores de los Topicos
     */
    private class NotasAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView titulo;
        TextView dependencia;
        TextView fecha;

        NotasAdapterViewHolder(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.adapter_topicos_item_titulo);
            dependencia = (TextView) itemView.findViewById(R.id.adapter_topicos_item_dependencia);
            fecha = (TextView) itemView.findViewById(R.id.adapter_topicos_item_fecha);
        }
    }

    public interface NotaByTopicoListener {
        void descargarNota(long idNota);
        void descargarMasNotas();
    }
}
