package com.dof.gesformexico.dofsegob.Modelo;

/**
 * @author José Cruz Galindo Martínez on 10/02/17.
 */

public class GSCotizacionContainer {

    private String response;
    private int messageCode;
    public GSCotizacion cotizador;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }
}
