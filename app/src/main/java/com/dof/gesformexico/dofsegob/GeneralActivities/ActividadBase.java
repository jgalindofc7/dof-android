package com.dof.gesformexico.dofsegob.GeneralActivities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by José Cruz Galindo Martínez on 14/02/17.
 */

public class ActividadBase extends AppCompatActivity {

    protected ProgressDialog mDialog;

    protected void mostrarDialogo(String mensaje) {
        if (mDialog == null) {
            mDialog = new ProgressDialog(this);
            mDialog.setMessage(mensaje);
            mDialog.setCancelable(false);
            mDialog.show();
        } else {
            mDialog.setMessage(mensaje);
        }
    }

    protected void ocultarDialogo() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
