package com.dof.gesformexico.dofsegob.Home;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.dof.gesformexico.dofsegob.Busqueda.BusquedaAvanzadaActivity;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Sumario.ActivitySumario;
import com.dof.gesformexico.dofsegob.Topicos.TopicosActivity;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.dof.gesformexico.dofsegob.fecha.CustomDatePicker;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author José Cruz Galindo Martínez on 16/03/17.
 */
public class HomeFragment extends FragmentoBase implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.fragment_home_ejemplar_reciente)
    Button ejemplarBtn;
    @BindView(R.id.fragment_home_busqueda_fecha)
    Button busquedaFechaBtn;
    @BindView(R.id.fragment_home_busqueda_avanzada)
    Button busquedaAvanzadaBtn;
    @BindView(R.id.fragment_home_topicos)
    Button topicosBtn;

    public HomeFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, v);
        return v;
    }


    @OnClick(R.id.fragment_home_ejemplar_reciente)
    void mostrarEjemplarReciente() {
        Date hoy = Calendar.getInstance().getTime();
        Intent sumaryIntent = new Intent(getContext(), ActivitySumario.class);
        sumaryIntent.putExtra(Constants.DESERIALIZAR_FECHA, hoy.getTime());
        startActivity(sumaryIntent);
    }

    @OnClick(R.id.fragment_home_busqueda_fecha)
    void mostrarBusquedaFecha() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mostrarCalendarioForN();
        } else {
            mostrarCalendarioForL();
        }
    }

    @OnClick(R.id.fragment_home_busqueda_avanzada)
    void mostrarBusquedaAvanzada() {
        Intent busquedaIntent = new Intent(getContext(), BusquedaAvanzadaActivity.class);
        startActivity(busquedaIntent);
    }

    @OnClick(R.id.fragment_home_topicos)
    void mostrarTopicos() {
        Intent topicosIntent = new Intent(getContext(), TopicosActivity.class);
        startActivity(topicosIntent);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void mostrarCalendarioForN() {
        DatePickerDialog datePicker = new DatePickerDialog(getContext());
        datePicker.setOnDateSetListener(this);
        datePicker.show();
    }

    private void mostrarCalendarioForL() {
        CustomDatePicker datePickerF = new CustomDatePicker();
        datePickerF.setListener(this);
        datePickerF.show(getActivity().getFragmentManager(), "Seleccionar Fecha");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, dayOfMonth);
        Intent sumaryIntent = new Intent(getContext(), ActivitySumario.class);
        sumaryIntent.putExtra(Constants.DESERIALIZAR_FECHA, c.getTime().getTime());
        startActivity(sumaryIntent);
    }
}
