package com.dof.gesformexico.dofsegob.Sumario.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dof.gesformexico.dofsegob.Modelo.GSNota;
import com.dof.gesformexico.dofsegob.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * @author José Cruz Galindo Martínez on 13/02/17.
 */
public class SeccionAdapter extends RecyclerView.Adapter<SeccionAdapter.SeccionHolder> {

    private List<GSNota> listaNotas;
    private LayoutInflater mInflater;
    private NotaBySeccionListener mListener;
    private DateFormat sdf;

    public SeccionAdapter(Activity activity, List<GSNota> lista) {
        listaNotas = lista;
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es","MX"));
    }

    @Override
    public SeccionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = mInflater.inflate(R.layout.notas_item, parent, false);
        return new SeccionHolder(vista);
    }

    @Override
    public void onBindViewHolder(SeccionHolder holder, int position) {
        final GSNota slctd = listaNotas.get(position);
        holder.titulo.setText(slctd.titulo);
        String orga = slctd.codOrgaDos != null ? slctd.codOrgaDos : slctd.codOrgaUno;
        holder.dependencia.setText(orga);
        holder.fechas.setText(sdf.format(slctd.fecha));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.mandarNotaPorSeccion(slctd);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaNotas.size();
    }

    public void setListener(NotaBySeccionListener listener) {
        this.mListener = listener;
    }

    class SeccionHolder extends RecyclerView.ViewHolder {

        private TextView titulo;
        private TextView dependencia;
        private TextView fechas;

        SeccionHolder(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.notas_item_titulo);
            dependencia = (TextView) itemView.findViewById(R.id.notas_item_dependencia);
            fechas = (TextView) itemView.findViewById(R.id.notas_item_fecha);
        }
    }

    public interface NotaBySeccionListener {
        void mandarNotaPorSeccion(GSNota notaSelected);
    }
}
