package com.dof.gesformexico.dofsegob.Retrofit;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 */

public class ResultadoApi<E> {

    private boolean exito;
    private String motivo;
    private E objeto;

    public boolean isExito() {
        return exito;
    }

    public void setExito(boolean exito) {
        this.exito = exito;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public E getObjeto() {
        return objeto;
    }

    public void setObjeto(E objeto) {
        this.objeto = objeto;
    }
}
