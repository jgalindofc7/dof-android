package com.dof.gesformexico.dofsegob.Indicadores;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Indicadores.Adapter.ResultadoAdapter;
import com.dof.gesformexico.dofsegob.Modelo.GSIndicador;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author José Cruz Galindo Martínez on 24/04/17.
 */
public class ResultadoIndicadoresActivity extends AppCompatActivity {

    @BindView(R.id.actvty_result_indicadores_nombre)
    TextView nombreInd;
    @BindView(R.id.actvty_result_indicadores_total)
    TextView totalesInd;
    @BindView(R.id.actvty_result_indicadores_list)
    RecyclerView mRecycler;
    @BindView(R.id.actvt_result_fab_compartir)
    FloatingActionButton compartirBtn;
    @BindView(R.id.actvt_result_fab_descargar)
    FloatingActionButton descargarBtn;

    // Logic
    private List<GSIndicador> indicadores;
    private String indicadorSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado_indicadores);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.coti_toolbar);
        setSupportActionBar(myToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ButterKnife.bind(this);

        indicadores = new LinkedList<>();
        ResultadoAdapter mAdapter = new ResultadoAdapter(this, indicadores);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));

        indicadorSelected = getIntent().getStringExtra(Constants.DESERIALIZAR_INDICADOR_BUSQ);
        nombreInd.setText(indicadorSelected);
        Parcelable[] arr = getIntent().getParcelableArrayExtra(Constants.DESERIALIZAR_INDICADORES);
        GSIndicador nIndic;
        for (Parcelable p : arr) {
            nIndic = (GSIndicador)p;
            indicadores.add(nIndic);
        }
        String totalStr = String.format(new Locale("es","MX"), "Número de Resultados %d", indicadores.size());
        totalesInd.setText(totalStr);
        mAdapter.notifyDataSetChanged();
    }

    private String createFileCSV() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("es","MX"));
        StringBuilder sb = new StringBuilder();
        sb.append("Fecha, Valor\n");
        for (GSIndicador ind : indicadores) {
            sb.append(String.format("%s, %s\n",sdf.format(ind.getFecha()), ind.getValor()));
        }
        String nombre = String.format("dof_%s.csv", indicadorSelected);
        String path = Environment.getExternalStorageDirectory() + File.separator + nombre;
        File archivo = new File(path);
        try {
            boolean created = archivo.createNewFile();
            if (created) {
                OutputStream os = new FileOutputStream(archivo);
                os.write(sb.toString().getBytes());
                os.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return archivo.getAbsolutePath();
    }

    @OnClick(R.id.actvt_result_fab_descargar)
    void guardarArchivo() {
        String ruta = this.createFileCSV();
        Toast.makeText(this, "El archivo se ha guardado.", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.actvt_result_fab_compartir)
    void compartirArchivo() {
        Intent sIntent = new Intent();
        String ruta = this.createFileCSV();
        sIntent.setAction(Intent.ACTION_SEND);
        sIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(ruta)));
//        sIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(sIntent, "Compartir"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
