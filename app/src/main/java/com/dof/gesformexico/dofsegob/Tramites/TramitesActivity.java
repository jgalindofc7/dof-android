package com.dof.gesformexico.dofsegob.Tramites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dof.gesformexico.dofsegob.R;

/**
 * Created by José Cruz Galindo Martínez on 30/01/17.
 * Clase para mostrar los pdfs de tramites.
 */

public class TramitesActivity extends AppCompatActivity {

    private Toolbar toolBar;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tramites_activity);
        configureView();
    }


    private void configureView() {
        toolBar = (Toolbar) findViewById(R.id.tramites_toolbar);
        mViewPager = (ViewPager) findViewById(R.id.tramites_view_pager);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);

        setSupportActionBar(toolBar);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void configurarFragments() {
        TramitesAdapter adapter = new TramitesAdapter(getSupportFragmentManager());
        TramiteFragment fragment;
        fragment = new TramiteFragment();
        adapter.agregarFragmento(fragment);
        fragment = new TramiteFragment();
        adapter.agregarFragmento(fragment);
        fragment = new TramiteFragment();
        adapter.agregarFragmento(fragment);

        mViewPager.setAdapter(adapter);
    }
}
