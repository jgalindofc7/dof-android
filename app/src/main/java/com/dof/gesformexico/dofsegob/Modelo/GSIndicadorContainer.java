package com.dof.gesformexico.dofsegob.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 * Clase que representa el contenedor de indicadores.
 */

public class GSIndicadorContainer {

    private String response;
    private int messageCode;
    @SerializedName("ListaIndicadores")
    private List<GSIndicador> listaIndicadores;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public List<GSIndicador> getListaIndicadores() {
        return listaIndicadores;
    }

    public void setListaIndicadores(List<GSIndicador> listaIndicadores) {
        this.listaIndicadores = listaIndicadores;
    }


}
