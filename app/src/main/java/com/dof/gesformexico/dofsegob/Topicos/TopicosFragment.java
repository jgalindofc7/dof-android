package com.dof.gesformexico.dofsegob.Topicos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.ActivityDetalleNota;
import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.GeneralFragments.FragmentoBase;
import com.dof.gesformexico.dofsegob.Modelo.GSNotaContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSNotasTopicos;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.dof.gesformexico.dofsegob.Topicos.AdapterTopicos.NotaTopicosAdapter;
import com.dof.gesformexico.dofsegob.Utils.Constants;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author José Cruz Galindo Martínez on 03/02/17.
 */
public class TopicosFragment extends FragmentoBase  implements NotaTopicosAdapter.NotaByTopicoListener {

    // UI
    @BindView(R.id.content_topicos_recycler_view)
    RecyclerView mRecycler;

    private NotaTopicosAdapter notasAdapter;

    // Logic
    private List<GSNotasTopicos> listaTopicos = new LinkedList<>();
    private int paginaActual = 1;
    private boolean ascendente = true;
    private static final String ASCENDING = "ASC";
    private static final String DESCENDING = "DESC";

    public TopicosFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_topicos, container, false);

        notasAdapter = new NotaTopicosAdapter(getActivity(), listaTopicos);
        notasAdapter.setListener(this);

        ButterKnife.bind(this, rootView);

        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecycler.setAdapter(notasAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle bundle = getArguments();
        if (bundle != null) {
            GSNotasTopicos[] tmp = new Gson().fromJson(bundle.getString(Constants.DESERIALIZAR_NOTAS_TOPICOS),
                    GSNotasTopicos[].class);
            listaTopicos.addAll(Arrays.asList(tmp));
            notasAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_topicos, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_topicos_sort:
                ascendente = !ascendente;
                break;
            case R.id.menu_topicos_filter:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void descargarNota(long idNota) {
        final TopicosFragment weak = this;
        mostrarDialogo("Descargando Nota");
        Api.getSharedInstance().obtenerDetalleNota(idNota, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    GSNotaContainer wrapper = (GSNotaContainer) resultado.getObjeto();
                    Intent detailNotaIntent = new Intent(getActivity(), ActivityDetalleNota.class);
                    detailNotaIntent.putExtra(Constants.DESERIALIZE_NOTA, new Gson().toJson(wrapper.getNota()));
                    getActivity().startActivity(detailNotaIntent);
                    weak.ocultarDialogo();
                } else {
                    Toast.makeText(getContext(), "Error al descargar la nota",
                            Toast.LENGTH_SHORT).show();
                }
                weak.ocultarDialogo();
            }
        });
    }

    @Override
    public void descargarMasNotas() {
        paginaActual++;
    }
}
