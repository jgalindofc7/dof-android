package com.dof.gesformexico.dofsegob.Sumario.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Sumario.SumarioFragment;

import java.util.List;

/**
 * @author José Cruz Galindo Martínez on 09/02/17.
 */
class SumarioAdapter extends SectionedRecyclerViewAdapter<SumarioAdapter.MainViewHolder> {

    private List<String> seccMat;
    private List<String> seccVesp;
    private List<String> seccExt;
    private LayoutInflater mInflater;
    final private String[] SECCIONES = {"MATUTINA", "VESPERTINA", "EXTRAORDINARIA"};
    final private int[] SECC_IMAGENES = {R.drawable.s_clock_dark, R.drawable.s_evening_dark, R.drawable.s_evening_dark};
    private SumarioFragment contenedor;

    SumarioAdapter(Activity activity, List<String> matutinas,
                          List<String> vespertinas, List<String> extras) {
        seccMat = matutinas;
        seccVesp = vespertinas;
        seccExt = extras;
        mInflater = activity.getLayoutInflater();
    }

    @Override
    public int getSectionCount() {
        int ediciones = 0;
        if (seccMat.size() > 0) {
            ediciones++;
        }
        if (seccVesp.size() > 0) {
            ediciones++;
        }
        if (seccExt.size() > 0) {
            ediciones++;
        }

        return ediciones;
    }

    @Override
    public int getItemCount(int section) {
        int items = 0;
        switch (section) {
            case 0:
                items = seccMat.size();
                break;
            case 1:
                items = seccVesp.size();
                break;
            case 2:
                items = seccExt.size();
                break;
        }
        return items;
    }

    @Override
    public void onBindHeaderViewHolder(MainViewHolder holder, int section) {
        holder.edicion.setText(SECCIONES[section]);
        holder.imagen.setImageResource(SECC_IMAGENES[section]);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, final int section, final int relativePosition, int absolutePosition) {
        switch (section) {
            case SumarioFragment.EDICION_MATUTINA:
                holder.titulo.setText(seccMat.get(relativePosition));
                break;
            case SumarioFragment.EDICION_VESPERTINA:
                holder.titulo.setText(seccVesp.get(relativePosition));
                break;
            case SumarioFragment.EDICION_EXTRAORDINARIA:
                holder.titulo.setText(seccExt.get(relativePosition));
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandarNotasBySeccion(section, relativePosition);
            }
        });
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.sumario_header_item;
                break;
            case VIEW_TYPE_ITEM:
                layout = R.layout.sumario_seccion_item;
                break;
            default:
                layout = R.layout.sumario_seccion_item;
                break;
        }
        View v = mInflater.inflate(layout, parent, false);

        return new MainViewHolder(v);
    }

    public void setContainer(SumarioFragment container) {
        this.contenedor = container;
    }

    private void mandarNotasBySeccion(int seccion, int posicion) {
        switch (seccion) {
            case 0:
                Log.d("Sumario Adapter", "Seccion " + seccion + " Posicion " + posicion + seccMat.get(posicion));
                contenedor.mostrarNotasPorSeccion(seccion, seccMat.get(posicion));
                break;
            case 1:
                Log.d("Sumario Adapter", "Seccion " + seccion + " Posicion " + posicion + seccVesp.get(posicion));
                contenedor.mostrarNotasPorSeccion(seccion, seccVesp.get(posicion));
                break;
            case 2:
                Log.d("Sumario Adapter", "Seccion " + seccion + " Posicion " + posicion + seccExt.get(posicion));
                contenedor.mostrarNotasPorSeccion(seccion, seccExt.get(posicion));
                break;
        }
    }


    class MainViewHolder extends RecyclerView.ViewHolder {

        TextView titulo;
        ImageView imagen;
        TextView edicion;

        MainViewHolder(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.notas_seccion_titulo);
            imagen = (ImageView) itemView.findViewById(R.id.sumario_header_image);
            edicion = (TextView) itemView.findViewById(R.id.sumario_header_titulo);
        }
    }

}
