package com.dof.gesformexico.dofsegob.Modelo;

import com.dof.gesformexico.dofsegob.DB.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by José Cruz Galindo Martínez on 03/01/17.
 * Tipo de indicador contiene el nombre y el id del indicador.
 */

@Table(database = AppDatabase.class)
public class GSTipoIndicador extends BaseModel {

    @PrimaryKey
    @Unique
    @Column
    public String id;

    @Column
    public String value;


    @Override
    public String toString() {
        return "Id: " + this.id + " Valor: " + this.value;
    }

   /* public static void guardarORemplazar() {
        GSTipoIndicador tipoIndicador = SQLite.select().from(GSTipoIndicador.class)
                .where(GSTipoIndicador_Table.id.eq(indAux.id)).querySingle();
    }*/
}