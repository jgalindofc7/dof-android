package com.dof.gesformexico.dofsegob.Modelo;

/**
 * @author José Cruz Galindo Martínez on 08/05/17.
 */
public class GSEnvioContacto {

    private String mailFrom;
    private String subject;
    private String mailText;
    private String codConsultaTema;

    public GSEnvioContacto(String mailFrom, String mailText) {
        this.mailFrom = mailFrom;
        this.mailText = mailText;
        this.subject = "Mensaje de Contacto";
    }

    public GSEnvioContacto(String mailFrom, String subject, String mailText, String codConsultaTema) {
        this.mailFrom = mailFrom;
        this.subject = subject;
        this.mailText = mailText;
        this.codConsultaTema = codConsultaTema;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMailText() {
        return mailText;
    }

    public void setMailText(String mailText) {
        this.mailText = mailText;
    }

    public String getCodConsultaTema() {
        return codConsultaTema;
    }

    public void setCodConsultaTema(String codConsultaTema) {
        this.codConsultaTema = codConsultaTema;
    }
}
