package com.dof.gesformexico.dofsegob.Cotizador;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dof.gesformexico.dofsegob.Api.Api;
import com.dof.gesformexico.dofsegob.Modelo.GSCotizacionContainer;
import com.dof.gesformexico.dofsegob.Modelo.GSCotizador;
import com.dof.gesformexico.dofsegob.R;
import com.dof.gesformexico.dofsegob.Retrofit.ResultadoApi;
import com.dof.gesformexico.dofsegob.Retrofit.TaskCallback;
import com.google.common.io.ByteStreams;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by José Cruz Galindo Martínez on 01/02/17.
 * Clase para mandar a cotizar un documento.
 */
public class CotizadorFragment extends Fragment {

    // UI
    @BindView(R.id.fragment_cotizador_nombre_in)
    EditText nombre;
    @BindView(R.id.fragment_cotizador_apellido_in)
    EditText apellido;
    @BindView(R.id.fragment_cotizador_telefono_in)
    EditText telefono;
    @BindView(R.id.fragment_cotizador_correo_in)
    EditText correo;
    @BindView(R.id.fragment_cotizador_ruta)
    TextView nombreArchivo;

    // Logic
    private String archivoB64;
    private Unbinder unbinder;

    public static final int FILE_CHOOSER_ACTIVITY = 17;

    public CotizadorFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cotizador, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fragment_cotizador_explorar_btn)
    public void buscarArchivo() {
        mandarExploradorArchivos();
    }

    @OnClick(R.id.fragment_cotizador_cotizar_btn)
    public void mandarCotizacionServer() {
        if (datosValidos()) {
            mandarCotizacion();
        } else {
            Toast.makeText(getContext(), "Por favor revisa los datos.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private boolean datosValidos() {
        boolean valido = true;
        if (nombre.getText().toString().isEmpty() ) {
            nombre.setError("Campo Requerido");
            valido = false;
        }
        if (apellido.getText().toString().isEmpty() ) {
            apellido.setError("Campo Requerido");
            valido = false;
        }
        if (telefono.getText().toString().isEmpty() ) {
            telefono.setError("Campo Requerido");
            valido = false;
        }
        // Verificar que sea el formato
        if (correo.getText().toString().isEmpty() ) {
            correo.setError("Campo Requerido");
            valido = false;
        }
        // verificar que sea el formato correcto
        return valido;
    }

    private void mandarExploradorArchivos() {
        Intent intentFC = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intentFC.setType("*/*");
        String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"};
        intentFC.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        intentFC.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intentFC, "Selecciona un Archivo"),
                    FILE_CHOOSER_ACTIVITY);
        } catch (ActivityNotFoundException acnfe) {
            Log.e("CotizadorFragment", "No hay manejador de archivos");
        }
    }

    private void mandarCotizacion() {
        GSCotizador cotizador = new GSCotizador();
        cotizador.setNombres(nombre.getText().toString());
        cotizador.setApellidos(apellido.getText().toString());
        cotizador.setTelefono(telefono.getText().toString());
        cotizador.setEmail(correo.getText().toString());
        cotizador.setFileName(nombreArchivo.getText().toString());
        cotizador.setTipoCotizacion("AVISO");
        cotizador.setArchivoB64(this.archivoB64);

        Log.d("CotizadorFragment", "Voy a mandar " + cotizador.toString());

        Api.getSharedInstance().mandarArchivoCotizacion(cotizador, new TaskCallback() {
            @Override
            public void processResult(ResultadoApi resultado) {
                if (resultado.isExito()) {
                    Log.d("CotizadorFragment", "Tuve Exito");
                    GSCotizacionContainer wrapper = (GSCotizacionContainer)resultado.getObjeto();
                    Toast.makeText(getContext(), wrapper.cotizador.getCodigo(), Toast.LENGTH_LONG).show();
                } else {
                    Log.e("CotizadorFragment", "Fracase");
                    Log.e("CotizadorFragment", resultado.getMotivo());
                    Toast.makeText(getContext(), "Ocurrio un Error", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILE_CHOOSER_ACTIVITY && resultCode == Activity.RESULT_OK) {
            Uri fileUri = data.getData();
            try {
                archivoB64 = readTextFromUri(fileUri);
                System.out.println(archivoB64);
                nombreArchivo.setText(obtenerFileName(fileUri));
            } catch (IOException ioe) {
                Log.e("CotizadorFragment", ioe.getMessage());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
        if (inputStream != null) {
            byte[] bytesArry = ByteStreams.toByteArray(inputStream);
            return Base64.encodeToString(bytesArry, Base64.DEFAULT);
        }
        return null;
    }

    private String obtenerFileName(Uri uri) {
        Cursor cursor;
        String salida = "";
        cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst() ) {
            salida = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
        }
        Log.d("CotizadorFragment", salida);
        assert cursor != null;
        cursor.close();
        return salida;
    }


}
