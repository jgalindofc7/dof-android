package com.dof.gesformexico.dofsegob.Retrofit;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 */

public interface TaskCallback<E> {

    void processResult(ResultadoApi<E> resultado);
}
