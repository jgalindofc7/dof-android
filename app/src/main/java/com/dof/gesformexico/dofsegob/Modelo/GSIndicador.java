package com.dof.gesformexico.dofsegob.Modelo;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

/**
 * Created by José Cruz Galindo Martínez on 29/12/16.
 * Clase que es un modelo de un indicador.
 */

public class GSIndicador extends BaseModel implements Parcelable {

    @PrimaryKey
    private long codIndicador;
    private long codTipoIndicador;
    private long estaFecha;
    private Date fecha;
    private String usuario;
    private String valor;

    public long getCodIndicador() {
        return codIndicador;
    }

    public void setCodIndicador(long codIndicador) {
        this.codIndicador = codIndicador;
    }

    public long getCodTipoIndicador() {
        return codTipoIndicador;
    }

    public void setCodTipoIndicador(long codTipoIndicador) {
        this.codTipoIndicador = codTipoIndicador;
    }

    public long getEstaFecha() {
        return estaFecha;
    }

    public void setEstaFecha(long estaFecha) {
        this.estaFecha = estaFecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.codIndicador);
        dest.writeLong(this.codTipoIndicador);
        dest.writeLong(this.estaFecha);
        dest.writeLong(this.fecha != null ? this.fecha.getTime() : -1);
        dest.writeString(this.usuario);
        dest.writeString(this.valor);
    }

    public GSIndicador() {
    }

    protected GSIndicador(Parcel in) {
        this.codIndicador = in.readLong();
        this.codTipoIndicador = in.readLong();
        this.estaFecha = in.readLong();
        long tmpFecha = in.readLong();
        this.fecha = tmpFecha == -1 ? null : new Date(tmpFecha);
        this.usuario = in.readString();
        this.valor = in.readString();
    }

    public static final Parcelable.Creator<GSIndicador> CREATOR = new Parcelable.Creator<GSIndicador>() {
        @Override
        public GSIndicador createFromParcel(Parcel source) {
            return new GSIndicador(source);
        }

        @Override
        public GSIndicador[] newArray(int size) {
            return new GSIndicador[size];
        }
    };
}
