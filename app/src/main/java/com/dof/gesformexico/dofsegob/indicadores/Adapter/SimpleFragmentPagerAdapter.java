package com.dof.gesformexico.dofsegob.Indicadores.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dof.gesformexico.dofsegob.Indicadores.Fragment.IndicadoresFragment;
import com.dof.gesformexico.dofsegob.Indicadores.Fragment.SearchFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gersfor on 18/12/16.
 * Francisco Javier  Flores Morales
 * fflores.mo@gesfor.com.mx
 */
public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentos = new ArrayList<>(2);
    private String[] tabTitles = {"Recientes","Historico"};

    public SimpleFragmentPagerAdapter(FragmentManager fm){
        super(fm);

        IndicadoresFragment fragmOne = new IndicadoresFragment();
        SearchFragment fragmTwo = new SearchFragment();
        fragmentos.add(fragmOne);
        fragmentos.add(fragmTwo);
    }

    @Override
    public Fragment getItem(int position){
        return fragmentos.get(position);
    }

    @Override
    public int getCount(){
        return fragmentos.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        return tabTitles[position];
    }
}