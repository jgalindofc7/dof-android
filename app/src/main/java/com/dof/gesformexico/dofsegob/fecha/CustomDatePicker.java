package com.dof.gesformexico.dofsegob.fecha;

import android.app.DatePickerDialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import java.util.Calendar;

/**
 * Created by Gersfor on 18/12/16.
 * Francisco Javier  Flores Morales
 * fflores.mo@gesfor.com.mx
 */
public class CustomDatePicker extends DialogFragment {

    private DatePickerDialog.OnDateSetListener mListener;

    public CustomDatePicker() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int dia = c.get(Calendar.DAY_OF_MONTH);
        int mes = c.get(Calendar.MONTH);
        int anio = c.get(Calendar.YEAR);

        DatePickerDialog dpd = new DatePickerDialog(getActivity(), mListener, anio, mes, dia);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
        return dpd;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.mListener = listener;
    }

}